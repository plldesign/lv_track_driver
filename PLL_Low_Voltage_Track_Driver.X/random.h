/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           random.h
 *
 * About:
 *  Random number generator definitions.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Repackaged and modified random function
 *                                       to better suit the modular architecture.
 ******************************************************************************/


#ifndef RANDOM_H
#define	RANDOM_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>


/** \brief Initialise random number generator.
 *
 * This implementation of the random number generator uses the stdlib rand()
 * function which needs to be initialised with a call to srand() with a 16bit
 * seed.
 */
void random_init();


/** \brief Return a random byte.
 *
 * The random number is needed for random search address generation such that
 * each device on the bus gets assigned a unique short address after the
 * commissioning process. As such, the number should be as random as possible.
 * This implementation just calls the stdlib rand() function.
 *
 * @return A random byte.
 */
uint8_t random_byte();




#ifdef	__cplusplus
}
#endif

#endif	/* RANDOM_H */

