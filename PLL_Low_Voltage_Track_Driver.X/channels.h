/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _CHANNELS_H    /* Guard against multiple inclusion */
#define _CHANNELS_H

#include "mcc_generated_files/mcc.h"
#include "dimcurves.h"

#define NUMBER_OF_CHANNELS 1
#define CH0 dimChannel[0]
#define CHANNEL_WEIGHT_MIN 0.1
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
typedef struct {
    uint8_t ChannelNo;
    uint32_t dimVal;// = 0;//32 bit to deal with over flow conditions due to float>int conversion
    uint16_t minLevel;//Minimum turn on level,
    float channelWeight; //percentage of master dim level for channel, calc'd using envelope
    transferCoordinate_t * envelope;
    void (*PWMdualCompareValueSet)(uint16_t, uint16_t);
    void (*IdualCompareValueSet)(uint16_t, uint16_t);
    void (*enable)(void);
} DIMCHANNEL_t;

DIMCHANNEL_t dimChannel[NUMBER_OF_CHANNELS];

/*-------------------------Channels Setup ------------------------------------*/

void channel_init(void);

void CH0_PWM_stop(void);

void CH0_PWM_start(void);


void channelSet(uint16_t PWMvalue, uint16_t Ivalue, DIMCHANNEL_t *dimchannel);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _CHANNELS_H */

/* *****************************************************************************
 End of File
 */
