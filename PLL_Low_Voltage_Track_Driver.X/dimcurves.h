/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _dimcurves_H    /* Guard against multiple inclusion */
#define _dimcurves_H

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "mcc_generated_files/mcc.h"
#include <stdio.h>

#define FLOAT_SIZE 4 //float has 4 bytes in memory for XC32
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
   
/*-------------------------New Curve Expansion Stuff--------------------------*/  
typedef struct transferCoordinate{float input; float output;} transferCoordinate_t;

extern const transferCoordinate_t CIE_UNIT_CURVE[502];


float floatCurveExpand(transferCoordinate_t * curve, float dimVal);

void printCurve (transferCoordinate_t * curve);

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _dimcurves_H */

/* *****************************************************************************
 End of File
 */
