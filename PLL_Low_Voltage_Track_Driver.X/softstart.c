#include <stdbool.h>
#include "softstart.h"
#include "mcc_generated_files/pin_manager.h"
#include "analog.h"

bool is_undervoltage(void){
    if (UVLO_VAL > getAnalogSample(ADC1_VIN))
    {
        return true;
    }
    else
    {
        return false;
    }
} 

void release_soft_start(void){
    SOFT_START_PIN = 1;
    
}

