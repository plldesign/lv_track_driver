/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _ANALOG_H   /* Guard against multiple inclusion */
#define _ANALOG_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include <stdio.h>
#include "mcc_generated_files/mcc.h"

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

#define ADC_trigger_time 300
#define ADC_NB_SAMPLE_AVERAGE 512
   
    
typedef enum ADC1_SM_states{
    ADC1_START,
    ADC1_CHANNEL_SELECT,
    ADC1_TRIGGER_SAMPLE,
    ADC1_TASKS,
    ADC1_PROCESS_SAMPLE,
    ADC1_RESTART
} ADC_SM_states_t;

typedef enum ADC1_CHANNEL_states{
    TEMPERATURE_START,
    TEMPERATURE_PROCESS,
    TEMPERATURE_DONE,
    SYS_VOLT_START,
    SYS_VOLT_PROCESS,
    SYS_VOLT_DONE
} ADC1_CHANNEL_states_t;

typedef struct adc{
ADC_SM_states_t SM_state;//= ADC1_START;
ADC1_CHANNEL_states_t Channel_state;
ADC1_CHANNEL channel;// = ADC1_TEMP;
bool sample_ready;//  = false;
bool run;//      = false;
uint16_t wait_timer;//       = 0; 
uint16_t conversion;//       = 0;
} ADC_t ;

ADC_t ADC1;

void ADCISR_fired(void);

uint16_t getAnalogSample(ADC1_CHANNEL channel);

void ADC1_NB_SM(void);

uint16_t get_NB_Vtemperature(void);




    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _BEAMWIDTH_H */

/* *****************************************************************************
 End of File
 */
