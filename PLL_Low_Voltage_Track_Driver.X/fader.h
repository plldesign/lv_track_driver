/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _TRANSITION_FADER_H    /* Guard against multiple inclusion */
#define _TRANSITION_FADER_H

#define MASTER_FADER    fader[0]
#define VB_FADER        fader[1]
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "./mcc_generated_files/mcc.h"


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
   
    
typedef enum fader_state{
        fader_calc_abs,
        fader_transition_idle,
        fader_transition_up,
        fader_transition_down,
        fader_transition_at_target
}fader_state_t;

typedef union _fader_status_flags
{
    unsigned int    all;
    
    struct status_flags {
    unsigned int	fading:1;
    unsigned int	pwmDimming:1;
    unsigned int	linearMode:1;
    unsigned int	reserved3:1	;
    unsigned int	reserved4:1	;
    unsigned int	reserved5:1	;
    unsigned int	reserved6:1	;
    unsigned int	currentDimming:1;
    unsigned int	reserved8:1	;
    unsigned int	reserved9:1	;
    unsigned int	reserved10:1;
    unsigned int	reserved11:1;
    unsigned int	reserved12:1;
    unsigned int	reserved13:1;
    unsigned int	reserved14:1;
    unsigned int	reserved15:1;
    }set_bit;
} fader_status_flags; 
    
typedef union _fader_config_flags
{
    unsigned int    all;
    struct config_flags {
    unsigned int    in_use:1;
    unsigned int    to_be_captured:1;
    unsigned int    to_be_restored:1;
    unsigned int    to_be_homed:1;
    unsigned int 	reserved4:1;
    unsigned int 	reserved5:1;
    unsigned int	reserved6:1;
    unsigned int	reserved7:1;
    unsigned int	reserved8:1;
    unsigned int	reserved9:1;
    unsigned int	reserved10:1;
    unsigned int	reserved11:1;
    unsigned int	reserved12:1;
    unsigned int	reserved13:1;
    unsigned int	reserved14:1;
    unsigned int	reserved15:1;
    }set_bit;
}  fader_config_flags;
    
typedef struct fader_config{
    char        index_character; // A/W/K (HUE AND SAT LATER)
    uint16_t    limit_min;//*Query values 
    uint16_t    limit_max;//*
    uint16_t    home_state;
    uint16_t    function_ID;
    bool        linear_flag;
    
    fader_status_flags status_flags;
    fader_config_flags config_flags;
    
}fader_config_t;



typedef struct transition_fader{
    fader_config_t config;
    
    bool enable;// = true;
    volatile float value;// = 1;
    
    volatile uint16_t ABSvalue;
    volatile uint16_t ABStransitionrate;
    
    float previous_value;// = 0;
    float target;// = 0;
    float increment;// = 0;
    volatile fader_state_t state;// = fader_transition_idle;
}transition_fader_t;


transition_fader_t fader[4];


void fader_init(transition_fader_t * fader);


void fader_set(float new_fader_val, float transitionRate, transition_fader_t * fader);
void fader_set_SC(float new_fader_val, float transitionRate, transition_fader_t * fader);

uint16_t absolute_to_fader_convert(uint16_t new_abs_fader_val, transition_fader_t * fader);
void set_faderABS(uint16_t ABSval, uint16_t transitionRate, transition_fader_t * fader);
uint16_t get_absolute(transition_fader_t * fader);

void fader_set_16bit(uint16_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader);
void fader_set_16bit_SC(uint16_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader);
void fader_set_dali(uint8_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader);


void fader_SM(transition_fader_t * fader);
bool is_fader_active(transition_fader_t * fader);


float get_fader_val(transition_fader_t * fader);
uint16_t get_fader_val_16bit(transition_fader_t * fader);
uint16_t get_fader_val_dali(transition_fader_t * fader);

float get_fader_target(transition_fader_t * fader);
uint16_t get_fader_target_16bit(transition_fader_t * fader);

void fader_halt(transition_fader_t * fader);

float fader_getStore(transition_fader_t * fader);
void fader_store(transition_fader_t * fader);
void fader_setDisabled(transition_fader_t * fader);
void fader_setEnabled(transition_fader_t * fader);



    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _TRANSITION_FADER_H */

/* *****************************************************************************
 End of File
 */
