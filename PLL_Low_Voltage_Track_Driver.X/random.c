/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           random.c
 *
 * About:
 *  Random number generator. This uses the stdlib rand() function, seeded with
 *  some values read from unconnected ADC inputs on the board.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Repackaged and modified random function
 *                                       to better suit the modular architecture.
 ******************************************************************************/


#include <stdlib.h>
#include "analog.h"
#include "random.h"

/** \brief Obtain a truly random number.
 *
 * This is used to initialise the pseudo-random number generator included in
 * stdlib. The implementation generates a random number by sampling AN9,10
 * (RF4,5) which on the demo board are not connected.
 *
 * @return A random number.
 */
static uint8_t random_trulyRandomNumber();


/**********************Function implementations********************************/

void random_init()
{
//    uint16_t u = ((random_trulyRandomNumber() << 8) | random_trulyRandomNumber());
//    printf("r%u\n", u);
    srand((random_trulyRandomNumber() << 8) | random_trulyRandomNumber());
}

uint8_t random_byte()
{
    return (uint8_t)rand();
}


static uint8_t random_trulyRandomNumber(void){
    uint8_t i = 0, mess = 0;
    do{
        
        for(i=0; i<20; i++)
        {
         mess += (uint8_t) getAnalogSample(ADC1_NOISE); 
         mess += (uint8_t) getAnalogSample(ADC1_VIN);
        }
        
    } while (mess == 0 || mess == 0xFF);
//    printf("%u mess\n", mess);
    return mess;
}

/**********************Function implementations*(END)**************************/
