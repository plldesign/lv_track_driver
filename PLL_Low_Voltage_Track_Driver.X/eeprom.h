/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _EEPROM_H    /* Guard against multiple inclusion */
#define _EEPROM_H

/*
 *
 */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
#include <stdbool.h>


#define     lo8(x) ((x)&0xff)
#define     hi8(x) ((x)>>8)

#define M24256_RETRY_MAX            300  // define the retry timeout (ms)
#define M24256_SLAVE_MAX            300 // define slave response timeout(ms)
#define M24256_ADDRESS              0b1010000 // 7 bit slave device address

// Note these are aligned with the I2C driver.


// We've added these errors, these occur seperately to i2c errors
#define ERR_REQ_OVERFLOW		0x80
#define ERR_REQ_ADD_ERR			0x82
#define ERR_REQ_PAGE_WR_ERR		0x83
#define ERR_REQ_BUSY			0x81		// if you get busy then retry - this in a non fatal state.


#define EEP_PAGE_SIZE       	0x40			// Do not try and write across pages!
#define EEP_MAX_SIZE       		0x40			//
#define EEP_FIRST_ADD			0x0000			// First address we allow write and reads too
#define EEP_LAST_ADD			0x3FFF			// Last address we allow write and reads too.


    
/* EEPROM address space usage */
//
//
//#define EEP_GROUP_START			0x00000400			//
//#define EEP_FIRST_SCN				0x00001000			// 		Scenes start at 0x00001000
    
#define EE_DALI_INITIALISE          0x0 //All addresses are set to 0xff when eeprom is new, use this field to detect blank eeprom
                                        //BANK 0 is held in program memory - is const

#define EE_DALI_BANK1_START_ADDR            0x1 
#define EE_DALI_BANK1_LEN                   0x10 //BANK 1 last addr 0x10 - Same as
    
#define EE_DALI_BANK2_START_ADDR            0x40 
#define EE_DALI_BANK2_LEN                   0x20 //BANK 2 last addr 0x60
    
#define EE_DALI_BANK_MACHINE_START_ADDR     0x80    //start on second page so not to write over page end
#define EE_DALI_BANK_MACHINE_LEN            0x31   //INCLUDES SCENES, last addr 0xB1

#define EE_DALI_LAST_ARC_START_ADDR         0xC0// 120
#define EE_DALI_LAST_ARC_LEN                0x20//LAST ARC, last addr 0xE0

#define EE_DALI_BANK208_START_ADDR            0x100 
#define EE_DALI_BANK208_LEN                   0x5 //BANK 2 last addr 0x105    
    
/* Direct access addresses*/
#define EE_DALI_CURRENT_SETTING                 0x103//160
#define EE_DALI_DIMCURVE_OFFSET                 0x104
    
//void eepInit(void);
void EEP_ISR_1Khz(void);
uint8_t EEP_Write(uint16_t address, uint8_t length, uint8_t *data);
bool EEP_Random_Read(uint16_t address,  uint8_t length, uint8_t *data);
void eepReset(void);
void eepTest(void);

#ifdef __cplusplus
}
#endif

#endif /* _EEPROM_H */

/* *****************************************************************************
 End of File
 */
