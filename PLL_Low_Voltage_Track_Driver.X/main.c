/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.145.0
        Device            :  PIC32MM0256GPM028
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36b
        MPLAB 	          :  MPLAB X v5.25
*/

/*
    (c) 2019 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/
/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#include "channels.h"
#include "fader.h"
#include "LEoutputProcessor.h"
#include "eeprom.h"
#include "dali/dali_cg.h"
#include "random.h"
#include "analog.h"
#include "softstart.h"

/*
                         Main application
 */
int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    random_init();
    channel_init();
    fader_init(&MASTER_FADER);
    
//    nvmem_reset(); // resets first value of eeprom to 0xFF - for debug only.
    nvmem_dali_banks_init();// only runs if first value in eeprom is 0xFF 
    dalihw_init();//d
    dali_init();//d
    tdali_flags_cg daliCGFlags;//d
    daliCGFlags.All = 0;//d
    
   while(is_undervoltage())// May need to make the softstart resistor higher value 5-10K maybe.
   { 
       WATCHDOG_TimerClear();// clear WDT by performing a 16-bit write to WDTCON (WDTCLRKEY) Watchdog set to timeout if stuck for approx. 4s
       printf("UVLO\n");//Hang in here while UVLO on startup - UVLO not currently handled while running
   }
   release_soft_start();// System voltage is over 21V so start dimming.
    
    printf("init\n");
    while (1)
    {
        WATCHDOG_TimerClear();// clear WDT by performing a 16-bit write to WDTCON (WDTCLRKEY) Watchdog set to timeout if stuck for approx. 4s
//        printf("%u\n", getAnalogSample(ADC1_VIN));
        
        
        
        // Call DALI Library mainline code.
        dali_tasks(); //d
        // Read flags from DALI subsystem. These are cleared (in the subsystem)
        // as soon as they're read. Bring anything "new" by ORing with the
        // previous saved state. Whenever we've finished with a flag, we'll
        // clear it in this copy as well.
        daliCGFlags.All |= dali_getFlags();//
        
        
        lightup();
    }
    return 1; 
}
/**
 End of File
*/