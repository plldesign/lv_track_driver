#include "LEoutputProcessor.h"
#include "fader.h"
#include "dimcurves.h"
#include "channels.h"

volatile bool dimISR_active = false;
/*-------------------------VB & Master Dim Channel Mixer----------------------*/

void dimISR_fired(void)
{
    dimISR_active = true;
}

bool dimISR_actions(void)
{
    static uint16_t divider = 0;
    
    if(true == dimISR_active)
    {
        dimISR_active = false;
            if(10 >= ++divider)
            {
                divider = 0;
                fader_SM(&fader[0]);//Don't care about the application so just run all SMs
//                fader_SM(&fader[1]);
                return true;
            }
    }
        return false;
}

uint16_t roundf2u(float f)
{
    return (uint16_t)(f+0.5f);
}

uint16_t range16bit(float f)
{//converts unit float (0-1) to 16bit range (0-65535)
    return roundf2u(f * 65535);
}

inline uint16_t outputCalc(DIMCHANNEL_t * channel){
    float out = channel->channelWeight * floatCurveExpand(CIE_UNIT_CURVE, get_fader_val(&MASTER_FADER));
//    float out =  floatCurveExpand(CIE_UNIT_CURVE, get_fader_val(&MASTER_FADER));
//    printf("%.2f\n", get_fader_val(&MASTER_FADER));
//    printf("%u\n", range16bit(out));
    return range16bit(out);
}

inline float outputCalc_f(DIMCHANNEL_t * channel){

//    return channel->channelWeight * floatCurveExpand(CIE_UNIT_CURVE, get_fader_val(&MASTER_FADER));
    return floatCurveExpand(CIE_UNIT_CURVE, get_fader_val(&MASTER_FADER));
}

void mixedModeSwitch(uint16_t output, DIMCHANNEL_t * channel){
    uint16_t PWM = output;
    uint16_t Iset = 0xffff;
//    uint16_t PWM = 0x3fff;
//    uint16_t Iset = output;

    
    if (output >  0x3fff){// DIM PWM Freq should be
       
        PWM = 0x3fff;
        Iset = output;
    } 
    else{
        PWM = output;
        Iset = 0x3fff;
    }
   
    channelSet(PWM, Iset, channel);
}

void mixedModeSwitch_f(float output, DIMCHANNEL_t * channel){
    output = channel->channelWeight * output;
    uint16_t PWM = output;
    uint16_t Iset = 0xffff;
//    uint16_t PWM = 0x3fff;
//    uint16_t Iset = output;

    
    if (output >  0.4){
        PWM = 0x7fff;
        
        Iset = output * 0xffff;
    } 
    else{
        PWM = 2.5 * output * 0x7fff;
        
        Iset = 0.4 * 0xffff;
    }
 
    channelSet(PWM, Iset, channel);
}


//void LVout(void){ //makes output from VB_FADER for LV PWM connection
//
//        float out = get_fader_val(&VB_FADER) * 0x6F9A;//MCCP rollover value for 420h
//        
////        printf("%f.0\n", out);
//        if((uint16_t)out <= 0x7){
//            out = 0x7;
//        }
//         if((uint16_t)0x6F80 <= out){
//            out = 0x6F80; // MCCP rolls over at 0x6F9A
//        }
////        printf("%f\n", out);
//        
//        LVSet((uint16_t)out);
//}

//void LEDout(void){
//        uint32_t output = outputCalc(&CH0) + CH0.minLevel;
////    printf("%u\n", output);
////
//    if (output > 0xFFFF){
//        output = 0xFFFF;
//    }
//    
//    if (CH0.minLevel >= output){
//        output = 0x0;
//    }
//    
//    float foutput = 1;
////    foutput = getThermalFoldbackScalar();
//    foutput = (float)output;
////    printf("%u\n", output);
//    mixedModeSwitch((uint16_t)output,&CH0);
//}

void LEDout(void){
      float output = outputCalc_f(&CH0) + (((float)CH0.minLevel)/0xffff);
//    printf("%f\n", (((float)CH0.minLevel)/0xffff));
//
    if (output > 1){
        output = 1;
    }
    
    if (((((float)CH0.minLevel)/0xffff) >= output) && (output != 0)){
        output = (((float)CH0.minLevel)/0xffff);
    }
    if (output == ((float)CH0.minLevel)/0xffff){
         output = 0;
    }


    mixedModeSwitch_f(output,&CH0);
}

void lightup(void){
    
    if(dimISR_actions())
    {
        if (((is_fader_active(&MASTER_FADER) )))// || is_thermal_foldback_active()))
        {   
            LEDout();
        }
        if(is_fader_active(&VB_FADER)){

//            LVout();
        }
    }

}