#include "analog.h"

uint16_t getAnalogSample(ADC1_CHANNEL channel){//BLOCKING ADC READ DO NOT USE WHILE NB version is running (CAUSES WDTimeout)
  
//    return ADC1_GetConversion(channel);
    uint32_t conversion = 0;
    ADC1_ChannelSelect(channel);
    AD1CON1bits.SAMP = 0;
//    ADC1_Start();
//    //Provide Delay
////    while(0 == AD1CON1bits.SAMP);
//    int i;
//    for(i=0;i <1000;i++)
//    {
//    }
//    ADC1_Stop();
    while(!ADC1_IsConversionComplete())
    {
//        printf("a%u\n", ADC1_IsConversionComplete());
        ADC1_Tasks();   
    }
    conversion =  ADC1_ConversionResultGet();
//        printf("%u", conversion);
    return conversion;
}


ADC_NB_init(void){
ADC1.SM_state= ADC1_START;
ADC1.Channel_state = TEMPERATURE_START;
ADC1.channel = ADC1_TEMP;
ADC1.sample_ready  = false;
ADC1.run      = false;
ADC1.wait_timer      = 0; 
ADC1.conversion      = 0; 
//printf("ADC NB init\n");
}


bool is_ADC1_sample_ready(void){
    return ADC1.sample_ready;
}

void start_ADC1_NB_SAMPLE(ADC1_CHANNEL channel){
    ADC1.channel = channel;
    ADC1.sample_ready = false;
    ADC1.run = true;
}

uint16_t get_ADC1_NB_sample(void){
    return ADC1.conversion;
}


uint32_t Vtemperature_acc = 0;
uint16_t Vtemperature = 1800;

uint16_t Vsystem_voltage = 0;


uint16_t sample_accumulate_count = 0;
bool sample_triggered = false; 
volatile bool ADCISR_active = false;


void ADCISR_fired(void){
    ADCISR_active = true;
}


uint16_t get_NB_Vtemperature(void){
    return Vtemperature;
}


void ADC1_NB_SM(void){
    static uint16_t ADC_timer = 0;
    
    if(true == ADCISR_active){
        ADC_timer++;
         ADCISR_active = false;
    }
    if(ADC_timer >= ADC_trigger_time){
        sample_triggered = true;
        ADC_timer = 0;
    }
    
    switch(ADC1.Channel_state){
        case TEMPERATURE_START:
            if(!sample_triggered){
                break;
            }
//            printf("TEMP_START\n");
            
            start_ADC1_NB_SAMPLE(ADC1_TEMP);
            ADC1.Channel_state = TEMPERATURE_PROCESS;
            break;
            
        case TEMPERATURE_PROCESS:
//            printf("TEMP_PROC\n");
            if (is_ADC1_sample_ready())
            {
                Vtemperature_acc += get_ADC1_NB_sample();
                sample_accumulate_count++;
            }
            if(ADC_NB_SAMPLE_AVERAGE <= sample_accumulate_count)
            {
                ADC1.Channel_state = TEMPERATURE_DONE;
            }
            break;
            
        case TEMPERATURE_DONE:
//            printf("TEMP_DONE\n");
            Vtemperature = (float)Vtemperature_acc / ADC_NB_SAMPLE_AVERAGE;
            Vtemperature_acc = 0;
//            printf("%u vtemp, %u\n", Vtemperature, sample_accumulate_count);
            sample_accumulate_count = 0;
            sample_triggered = false;
            ADC1.Channel_state = TEMPERATURE_START;
            break;   
        default:
//            printf("BUMS\n");
            break;
            
    }
            
    
    ADC1.wait_timer++;
    
    switch(ADC1.SM_state)
    {
        case    ADC1_START:
            if (true == ADC1.run)
            {
                ADC1.SM_state = ADC1_CHANNEL_SELECT;
            }
            break;
            
        case    ADC1_CHANNEL_SELECT:
            ADC1_ChannelSelect(ADC1.channel);
            ADC1.SM_state = ADC1_TRIGGER_SAMPLE;
            break;
            
        case    ADC1_TRIGGER_SAMPLE:
            AD1CON1bits.SAMP = 0;
            ADC1.SM_state = ADC1_TASKS; 
            break;
            
        case    ADC1_TASKS:
            if (!ADC1_IsConversionComplete()){
                ADC1_Tasks();
            }
            else{
                ADC1.SM_state = ADC1_PROCESS_SAMPLE;
                ADC1_Tasks();
            }
            break;
            
        case    ADC1_PROCESS_SAMPLE:
            ADC1.conversion = ADC1_ConversionResultGet();
            ADC1.sample_ready = true;
            ADC1.SM_state = ADC1_RESTART;
            break;
                
        case    ADC1_RESTART:
            ADC1.run = false;
            ADC1.SM_state = ADC1_START;
            break;
    }
    
}



