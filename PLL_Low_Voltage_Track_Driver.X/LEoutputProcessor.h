#ifndef _LE_OUTPUT_PROCESSOR_H    /* Guard against multiple inclusion */
#define _LE_OUTPUT_PROCESSOR_H


//#define CH4 dimchannel[4] //LensVector potential control method

#define MINIMUM_OUTPUT 0x00

#define dimmerOut   CH0.dimVal


#include "mcc_generated_files/mcc.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    
/*-------------------------VB & Master Dim Channel Mixer----------------------*/


void updateChannelWeights(float val);
void dimISR_fired(void);
bool dimISR_actions(void);
void lightup(void);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
