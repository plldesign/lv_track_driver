#include "channels.h"
#include "mcc_generated_files/sccp4_compare.h"
#include "mcc_generated_files/sccp5_compare.h"
#include "eeprom.h"
/*-------------------------Channels Setup ------------------------------------*/

void CH0_init(void){
    SCCP4_COMPARE_Initialize();
    SCCP5_COMPARE_Initialize();
}

void CH0_PWM_stop(void){
    SCCP5_COMPARE_Stop();
}

void CH0_PWM_start(void){
    SCCP5_COMPARE_Start();
}


void channel_init(void){

    CH0.PWMdualCompareValueSet = &SCCP5_COMPARE_DualEdgeBufferedConfig;//inside ring - - channel 0
    CH0.IdualCompareValueSet = &SCCP4_COMPARE_DualEdgeBufferedConfig;
    CH0.ChannelNo = 0;
//    CH0.channelWeight = 0.76; //0.845 = 1.3A//0.715; // 0.715= 1.1A, // 0.91 = 1.4A
    
    uint8_t data;
    EEP_Random_Read(EE_DALI_CURRENT_SETTING, 1, &data);
    CH0.channelWeight = ((float)data * 4) / 1000;
    if(1.0 < CH0.channelWeight){
        CH0.channelWeight = 1;
    }
    
    if (CHANNEL_WEIGHT_MIN > CH0.channelWeight){
        CH0.channelWeight = CHANNEL_WEIGHT_MIN;
    }
    
    EEP_Random_Read(EE_DALI_DIMCURVE_OFFSET, 1, &data);
    CH0.minLevel = 2 * data;
    CH0.dimVal = 0;
    CH0.enable = &CH0_init;
    
}

void channelSet(uint16_t PWMvalue, uint16_t Ivalue, DIMCHANNEL_t *dimChannel)
{   
//    printf("ChanS\n");
    dimChannel->IdualCompareValueSet(0, Ivalue); 
    dimChannel->PWMdualCompareValueSet(0, PWMvalue);
}

