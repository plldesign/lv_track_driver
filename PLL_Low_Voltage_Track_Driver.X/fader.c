#include "./mcc_generated_files/mcc.h"
#include "./fader.h"
#include "dali/dali_core/dali_cg_machine.h"
/*-------------------------Variable Beam Channel Mixer--------------------------
*/

void fader_init(transition_fader_t * fader){
    fader->target = 0;
    fader->value = 0;
    fader->ABSvalue = 0;
    fader->ABStransitionrate = 10;
    fader_set(1.0, 1.0, fader);
    fader_store(fader);
    fader_set(0, 0, fader);//force init off
}

void fader_set_SC(float new_fader_val, float transitionRate, transition_fader_t * fader)// transition rate is in hundredths of a second
{
    
    if (transitionRate < 1){
        transitionRate = 1;
    }
  
    transitionRate = 1/transitionRate;
      
    if (new_fader_val > fader->value){
        transitionRate = ((new_fader_val - fader->value) * transitionRate);
    }
    else{
        transitionRate = ((fader->value - new_fader_val) * transitionRate);
    }  
    
    if (new_fader_val < fader->value){
        fader->state = fader_transition_down;
        fader->increment = -(transitionRate);
    }
    else if (new_fader_val > fader->value){
        fader->state = fader_transition_up;
        fader->increment = transitionRate;
    }
    else
    {   
        fader->state = fader_transition_at_target;
        return;
    }
    fader->target = new_fader_val;  
}


void fader_set(float new_fader_val, float transitionRate, transition_fader_t * fader){// transition rate is in 10ms
    if (fader->enable = false){
        return;
    }
    if(transitionRate < 1){
        transitionRate = 1;
    }
    
 
    transitionRate = 1/(transitionRate);//faders are triggered by 10ms interrupt 
    if (new_fader_val < fader->value){
        fader->state = fader_transition_down;
        fader->increment = -(transitionRate);
    }
    else if (new_fader_val > fader->value){
        fader->state = fader_transition_up;
        fader->increment = transitionRate;
    }
    else{
       
        fader->state = fader_transition_at_target;
        return;
    }
    fader->target = new_fader_val;  
}

uint16_t absolute_to_fader_convert(uint16_t abs_val, transition_fader_t * fader){// converts absolute command value in fader level, K or degrees to output 0x0 - 0xFFFF
                                                                              // This method uses the config.limits to establish max and min
    if (abs_val <= fader->config.limit_min ){
        abs_val = fader->config.limit_min;
    }
     if (abs_val >= fader->config.limit_max ){
        abs_val = fader->config.limit_max;
    }
    float output_val;
    
    
    if (fader->config.limit_min < fader->config.limit_max)
    {
        output_val = (abs_val  - fader->config.limit_min) * ((float) 65535.0 / (fader->config.limit_max  - fader->config.limit_min));//Casting 0xFFFF very important here!!
    }
    else
    {
        output_val = (abs_val  - fader->config.limit_max) * ((float) 65535.0 / (fader->config.limit_min  - fader->config.limit_max));//Casting 0xFFFF very important here!!
    }
    
    return (uint16_t) output_val;                    
 
}


uint16_t get_absolute(transition_fader_t * fader){// returns absolute value from fader level
                                                                              
    return fader->ABSvalue;                    
 
}

void fader_set_16bit(uint16_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader){
    
    float float_fader_Val = ((1.52590219e-5) * new_fader_Val);

    fader_set(float_fader_Val, (float)transitionRate, fader);
 
}

void fader_set_dali(uint8_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader){
    if(255 != new_fader_Val)
    {
    float float_fader_Val = ((1.0/254.0) * new_fader_Val);
    fader_set(float_fader_Val, (float)transitionRate, fader);
    }
}


void fader_set_16bit_SC(uint16_t new_fader_Val, uint16_t transitionRate, transition_fader_t * fader){
    
    float float_fader_Val = ((1.52590219e-5) * new_fader_Val);

    fader_set_SC(float_fader_Val, (float)transitionRate, fader);
}

void set_faderABS(uint16_t ABSval, uint16_t transitionRate, transition_fader_t * fader){
   fader->ABSvalue = ABSval;
   fader->ABStransitionrate = transitionRate;
   fader->state = fader_calc_abs;

};

void fader_SM(transition_fader_t * fader){ //Channel Mixer Specific to variable beam configuration
    
    float new_fader_val = fader->value + fader->increment;
    
    switch(fader->state)
    {
        case fader_calc_abs:
            fader_set_16bit(absolute_to_fader_convert(fader->ABSvalue, fader), fader->ABStransitionrate, fader );
            break;
        
            case fader_transition_idle:
                fader->config.status_flags.set_bit.fading = 0;
                break;
                
            case fader_transition_up:
                if(new_fader_val > 1){//check for roll-over
                    new_fader_val = 1;
                }
                fader->config.status_flags.set_bit.fading = 1;
                if (new_fader_val >= fader->target){//check if at or over target
                    fader->value = fader->target;
                    fader->state = fader_transition_at_target;
                    break;
                    
                }
                fader->value = new_fader_val;
                break;
                
            case fader_transition_down:
                 if(new_fader_val < 0){//check for roll-over
                    new_fader_val = 0;
                 }
                 fader->config.status_flags.set_bit.fading = 1;
                 if (new_fader_val <= fader->target){//check if at or over target
                    fader->value = fader->target;
                    fader->state = fader_transition_at_target;
                    break;
                }
                 
                fader->value = new_fader_val;
                break;
                
            case fader_transition_at_target:
                fader->increment = 0;
                fader->state = fader_transition_idle;
                fader->config.status_flags.set_bit.fading = 0;
                printf("fader_@TARGET\n");
                break;
    }
} 

bool is_fader_active(transition_fader_t * fader)
{
    if (fader_transition_idle != fader->state){
//        printf("FA\n");
        return true;
    }
    else{
        return false;
    }
}

float get_fader_val(transition_fader_t * fader){
    return fader->value;
}

uint16_t get_fader_val_16bit(transition_fader_t * fader){
    return (uint16_t)((float)65535 * fader->value);
}

uint16_t get_fader_val_dali(transition_fader_t * fader){
    return (uint16_t)((float)254 * fader->value);
}

float get_fader_target(transition_fader_t * fader){
    return fader->target;
}

uint16_t get_fader_target_16bit(transition_fader_t * fader){
    return (uint16_t)((float)65535 * fader->target);
}

void fader_store(transition_fader_t * fader){
     fader->previous_value = get_fader_target(fader);
//     printf("fader_ STORED\n");
}

float fader_getStore(transition_fader_t * fader){
//    printf("fader_ RESTORED\n");
    return fader->previous_value;
}

void fader_setDisabled(transition_fader_t * fader){
    fader->enable = false;
}

void fader_setEnabled(transition_fader_t * fader){
    fader->enable = true;
}

void fader_halt(transition_fader_t * fader){
    fader->state = fader_transition_at_target;
//    fader_set(get_fader_val(fader), 0, fader);
}