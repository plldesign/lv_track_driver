/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_layer.c
 *
 * About:
 *  Intermediate layer linking the core of the DALI library to the application.
 *  It is this layer that provides the API to the user on one side, and the
 *  functionality needed by the libary core on the other.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 *
 * Author            Date			Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/

#include "../dali_cg.h"
#include "dali_cg_layer.h"

#include "dali_cg_machine.h"


/**********************DALI layer variables************************************/


/** \brief Flags that the library uses to communicate with the application.
 *
 * These flags are cleared whenever they're read, in order to prevent the
 * application from bringing in stale data.
 */
static volatile tdali_flags_cg daliFlags;


/** \brief Lamp power, as dictated by the DALI library
 *
 * The application uses dali_getPower() to obtain this value whenever the lamp
 * power needs to be updated.
 */
static uint8_t lampPower;


/** \brief Lamp status, as reported by the lamp hardware to the DALI library.
 *
 * The application uses dali_setStatus() to set this value.
 */
static uint8_t lampStatus;


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Lamp operating mode, as reported by the lamp hardware to the DALI
 * library.
 *
 * The application uses dali_setOperatingMode() to set this value.
 */
static uint8_t lampOperatingMode;


/** \brief Reference system power status, as reported by the lamp hardware to
 * the DALI library.
 *
 * The application uses dali_setReferenceSystemPowerStatuss() to set this value.
 */
static uint8_t referenceSystemPowerStatus = 0;


/** \brief Non-logarithmic physical minimum level, as reported by the lamp
 * hardware to the DALI library.
 *
 * The application uses dali_setNonLogPhysicalMinimum() (normally just once
 * during the initialisation) to set this value.
 */
static uint8_t nonLogPhysicalMinimumLevel;

#endif /* DALI_USE_DEVICE_TYPE_6 */


/**********************DALI layer variables*(END)******************************/


/**********************API Calls***********************************************/

inline void dali_init()
{
    lampPower = 0;
    daliFlags.All = 0;
    lampStatus = 0;
#ifdef DALI_USE_DEVICE_TYPE_6
    lampOperatingMode = 0;
    referenceSystemPowerStatus = REFERENCE_SYSTEM_POWER_RUN_OK;
    nonLogPhysicalMinimumLevel = 0;
#endif /* DALI_USE_DEVICE_TYPE_6 */

    // We need to initialise the DALI state machine as well
    idali_initMachine();
}


inline void dali_tick1ms()
{
    idali_tick1msMachine();
    daliFlags.tick1ms = 1;
}


uint8_t dali_getFlags()
{
    uint8_t retVal;
    // 1ms tick is being set by interrupt, thus we need to make sure we don't
    // clear the flag just after it's been set (but not yet read)
//    di();
    retVal = daliFlags.All;
    daliFlags.All = 0;
//    ei();
    return retVal;
}


uint8_t dali_getPower()
{
    return lampPower;
}


void dali_setStatus(uint8_t status)
{
    lampStatus = status;
}

#ifdef DALI_USE_DEVICE_TYPE_6


void dali_setOperatingMode(uint8_t mode)
{
    lampOperatingMode = mode;
}


void dali_setNonLogPhysicalMinimum(uint8_t level)
{
    nonLogPhysicalMinimumLevel = level;
}


void dali_setReferenceSystemPowerStatus(uint8_t status)
{
    referenceSystemPowerStatus = status;
}

#endif /* DALI_USE_DEVICE_TYPE_6 */



/**********************API Calls*(END)*****************************************/


void idali_setLampPower(uint8_t value, uint8_t nonLogDimming)
{
    lampPower = value;
    daliFlags.nonLogDimming = nonLogDimming;
    daliFlags.updatedLampPower = 1;
}


uint8_t idali_getLampStatus()
{
    return lampStatus;
}


void idali_startIdentificationProcedure()
{
    daliFlags.startIdentificationProcedure = 1;
}


#ifdef DALI_USE_DEVICE_TYPE_6

uint8_t idali_getLampOperatingMode()
{
    return lampOperatingMode;
}


uint8_t idali_getLampNonLogPhysicalMinimum()
{
    return nonLogPhysicalMinimumLevel;
}


void idali_startReferenceSystemPower()
{
    daliFlags.startReferenceSystemPower = 1;
}


void idali_stopReferenceSystemPower()
{
    daliFlags.stopReferenceSystemPower = 1;
}


uint8_t idali_getStatusReferenceSystemPower()
{
    return referenceSystemPowerStatus;
}

#endif /* DALI_USE_DEVICE_TYPE_6 */


