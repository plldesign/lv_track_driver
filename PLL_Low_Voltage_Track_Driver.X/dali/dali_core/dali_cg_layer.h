/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_layer.h
 *
 * About:
 *  Internal definitions for the DALI layer.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/

#ifndef DALI_CG_LAYER_H
#define	DALI_CG_LAYER_H

#ifdef	__cplusplus
extern "C" {
#endif



#include <xc.h>
#include <stdint.h>
#include "../dali_cg_config.h"


/** \brief Set the lamp power in the API layer.
 *
 * This function is called by the internal DALI state machine in order to change
 * the brightness of the lamp. When nonLogDimming == 1 the lamp power should map
 * linearly to the value. The user needs to grab this brightness using the API
 * and apply it to the lamp.
 *
 * @param value Requested lamp power, as delivered by the DALI machine [0..254]
 *              (0 -- off, 254 -- full)
 * @param nonLogDimming 0 for logarithmic dimming, 1 for linear dimming
 */
void idali_setLampPower(uint8_t value, uint8_t nonLogDimming);


/** \brief Obtain lamp status from API layer.
 *
 * The DALI state machine uses this function to obtain information about the
 * lamp status. Note that the DALI state machine does NOT switch off the lamp
 * (using a call to idali_setLampPower()) in response to these error conditions,
 * although it does report the lamp to be off where required by the standard. It
 * is the application's responsibility to keep the lamp operating safely.
 *
 * @return Lamp status, as a bitmask. The following constants define possible
 * states: \n
 *  LAMP_STATUS_OK \n
 *  LAMP_STATUS_OPEN_CIRCUIT \n
 *  LAMP_STATUS_SHORT_CIRCUIT \n
 *  LAMP_STATUS_LOAD_INCREASE \n
 *  LAMP_STATUS_LOAD_DECREASE \n
 *  LAMP_STATUS_THERMAL_OVEROAD \n
 *  LAMP_STATUS_THERMAL_SHUTDOWN
 */
uint8_t idali_getLampStatus();


/** \brief Request that the Control Gear start its identification procedure.
 *
 * This function sets a flag informing the application that the identification
 * procedure should be started.
 */
void idali_startIdentificationProcedure();


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Obtain lamp operating mode from API layer.
 *
 * Check lamp operating mode. The standard specifies multiple operating modes
 * but no criteria for selecting any of these modes. It is thus the
 * responsibility of the application to use whichever operating mode it needs.
 *
 * @return lamp operating mode, as one of the following constants: \n
 *  OPERATING_MODE_PWM_MODE_ACTIVE \n
 *  OPERATING_MODE_AM_MODE_ACTIVE \n
 *  OPERATING_MODE_CURRENT_CONTROLLED \n
 *  OPERATING_MODE_HIGH_CURRENT_PULSE
 */
uint8_t idali_getLampOperatingMode();

/** \brief Obtain lamp non-logarithmic physical minimum level from API layer.
 *
 * @return Lamp non-logarithmic physical minimum level.
 */
uint8_t idali_getLampNonLogPhysicalMinimum();



/** \brief Request that the Control Gear starts a reference system power
 * measurement.
 *
 * This function sets a flag informing the application that the reference system
 * power measurement should be started.
 */
void idali_startReferenceSystemPower();


/** \brief Request that the Control Gear aborts a reference system power
 * measurement.

 * This function sets a flag informing the application that the reference system
 * power measurement should be aborted.
 */
void idali_stopReferenceSystemPower();


/** \brief Obtain reference system power measurement status from API layer.
 *
 * @return Reference system power measurement status, as one of the following
 * values: \n
 *  REFERENCE_SYSTEM_POWER_FAILED -- Reference system power failed \n
 *  REFERENCE_SYSTEM_POWER_RUN_OK -- Reference system power has been run
 * successfully \n
 *  REFERENCE_SYSTEM_POWER_RUNNING -- Reference system power running
 */
uint8_t idali_getStatusReferenceSystemPower();

#endif /* DALI_USE_DEVICE_TYPE_6 */


#ifdef	__cplusplus
}
#endif

#endif	/* DALI_CG_LAYER_H */

