/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_protocol.h
 *
 * About:
 *  Internal definitions for the DALI data protocol.
 *
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      18 October 2013     Implemented the DALI library as a set
 *                                       of multiple loosely coupled layers.
 ******************************************************************************/

#ifndef DALI_CG_PROTOCOL_H
#define	DALI_CG_PROTOCOL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
#include "../dali_cg.h"


/** \brief Initialise DALI protocol state machine.
 * 
 * Clears all flags, and sets up the correct external interrupt edge that should
 * trigger an interrupt.
 */
inline void idali_initProtocol();


/** \brief Send a backward frame.
 *
 * This function configures the DALI protocol machine to send a backframe in
 * reply to a forward frame that was sent by the Control Device. Before
 * attempting to send the frame, it first checks if transmission is allowed.
 * If we are in the allowed window of transmission, it is started immediately,
 * if the window has not begun yet, the data is buffered and as soon as the
 * window opens transmission is started. If the window closed, nothing is
 * transmitted.
 *
 * @param data Payload of backward frame
 */
void idali_sendBackwardFrame(uint8_t data);

void dali_interruptTeTimer(void);
void dali_interruptExternal(void);
#ifdef	__cplusplus
}
#endif

#endif	/* DALI_CG_PROTOCOL_H */

