/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_protocol.c
 *
 * About:
 *  Core of the DALI data protocol implementation. This is the implementation
 *  of the machinery that transmits data on the DALI bus and reads data from it.
 *  It uses a timer, an external interrupt pin for reception and a GPIO for
 *  transmission.
 *
 *
 * Author            Date			Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      18 October 2013     Implemented the DALI library as a set
 *                                       of multiple loosely coupled layers.
 ******************************************************************************/



#include "dali_cg_protocol.h"

#include "../dali_cg_hardware.h"
#include "dali_cg_machine.h"


/**********************DALI Protocol definitions*******************************/


// Various timer values that need to be loaded into the timer register to
// produce a given delay until the timer overflows. These are based on the TE
// value #defined in dali_cg_hardware.h.
#define TE_VALUE                        (TE_TIMER_OVERFLOW_VAL - TE)
#define TEx4_VALUE                      (TE_TIMER_OVERFLOW_VAL - 4 * TE)
#define TEx7_VALUE                      (TE_TIMER_OVERFLOW_VAL - 7 * TE)
#define TEx15_VALUE                     (TE_TIMER_OVERFLOW_VAL - 15 * TE)


// Values that the timer will be compared to to check the elapsed time. During a
// DALI frame reception at each bus transition the software arms the Te timer
// to trigger an interrupt after 4 Te. If this happens, it will be considered a
// timeout (and unless this is the last bit reception will be aborted and an
// error will be signalled) since all delays within a frame should be at most 2
// Te. If the external interrupt is triggered during this 4 Te period, the timer
// register is compared to these 4 values and if it falls within
// [TE_MIN, TE_MAX] a single Te is considered to have elapsed, whereas if it
// falls within [TE2_MIN, TE2_MAX], 2 Te is considered to have elapsed.
// Otherwise, a reception error is signalled.
#define TE_MIN                          (TEx4_VALUE + 0.5 * TE)
#define TE_MAX                          (TEx4_VALUE + 1.3 * TE)
#define TEx2_MIN                        (TEx4_VALUE + 0.7 * 2 * TE)
#define TEx2_MAX                        (TEx4_VALUE + 1.3 * 2 * TE)


// DALI protocol state machine states
enum
{
    ST_IDLE,                        // Idle
    ST_SEND_BACKWARD_DATA,          // Sending reply to Control Device
    ST_RECEIVE_FWD_DATA,            // Receiving forward frame
    ST_WAIT_AFTER_CODE_VIOLATION,   // Detected a code violation and waits for 4 Te before becoming ready to receive again
    ST_RECEIVE_FWD_DATA_EXTRA_TE,   // Received forward frame ending in a '1', so we need to wait for one more Te
    ST_WAIT_FOR_SEND,               // Waiting minimum delay between forward and backward frame
    ST_CAN_SEND                     // Backward frame transmission is allowed
};


// The state machine sets this flag if the upper layers require a backward frame
// to be sent but we're currently in the ST_WAIT_FOR_SEND state. This tells the
// machine to start sending the frame as soon as we transition to ST_CAN_SEND.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned backwardFrameToSend                : 1;
        unsigned                                    : 7;
    };
}tdali_flags_protocol;

/**********************DALI Protocol definitions*(END)*************************/




/**********************DALI Protocol variables*********************************/


/** \brief The state the DALI protocol state machine is in.
 *
 * This is one if ST_IDLE, ST_SEND_BACKWARD_DATA, etc.
 */
static volatile uint8_t daliProtocolState;


/** \brief Data that has been received so far. */
static volatile uint16_t rxPacket;


/** \brief Internal variable used in the transmission and reception of DALI
 * data.
 */
static volatile uint8_t halfBitNumber;


/** \brief Shift register that outputs the data that needs to be transmitted. */
static volatile uint8_t txPacket;


/** \brief Data that needs to be transmitted back to the Control Device. */
static volatile uint8_t backwardFrame;


/** \brief Number of bits that have been received so far. */
static volatile uint8_t rxPacketLen;


/** \brief DALI protocol state machine flags variable.
 *
 * Implements just one bit that signals that there's data that needs to be
 * transmitted as soon as the transmission window opens.
 */
static volatile tdali_flags_protocol daliProtocolFlags;

/**********************DALI Protocol variables*(END)***************************/




/**********************Function implementations********************************/


inline void idali_initProtocol()
{
    // Start from ST_IDLE
    daliProtocolState = ST_IDLE;

    // Clear all flags
    daliProtocolFlags.All = 0;

    // Make sure the external interrupt is configured to trigger on the first
    // bus tranition. This depends on the state of the bus at initialisation.
    if (dalihw_isDALILineLow())
    {
//        printf("RI\n");
        dalihw_extInterruptSetRisingDALI();
    }
    else
    {
        dalihw_extInterruptSetFallingDALI();
    }
}


void dali_interruptTeTimer(void)
{
    if (daliProtocolState == ST_SEND_BACKWARD_DATA)
    {
        // We are now sending a backframe to the Control Device. This being the
        // timer overflow interrupt, we need to set new data on the bus.
        switch (halfBitNumber)
        {
            case 1:
                // START BIT, 2nd half
                // The timer keeps running, at this current time it couldn't
                // be too far away from 0, but in order to increase the accuracy
                // of the baud rate, we add to this value instead of replacing
                // it. This has the advantage of removing most of the delay from
                // the ISR vectoring up to this point
                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                dalihw_setDALILineHigh();
                break;
            case 2:
                // DATA BIT, 1st half
                // Now is the time to set the DALI bus to the proper level for
                // bit 0 of our data.
                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                if ((txPacket & 0x80) != 0)
                {
                    // Send a '1' bit, which starts with a DALI_LO
                    dalihw_setDALILineLow();
                }
                else
                {
                    // Send a '0' bit, which starts with a DALI_HI
                    dalihw_setDALILineHigh();
                }
                break;
            case 18:
            case 19:
            case 20:
            case 21:
                // STOP BIT 1, 1st half
                // STOP BIT 1, 2nd half
                // STOP BIT 2, 1st half
                // STOP BIT 2, 2nd half
                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                dalihw_setDALILineHigh();
                break;
            case 22:
                // Transmission finalised OK. Go back to ST_IDLE such that
                // we can receive the next frame
                dalihw_teTimerOff();
                daliProtocolState = ST_IDLE;
                break;
            default:
                // This case handles all the in-between (data) bits, with a
                // distinction between 1st half (even halfBitNumber) and 2nd
                // half (odd halfBitNumber) of a bit.
                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                if ((halfBitNumber & 0x01) == 0)
                {
                    // halfBitNumber is even, we are in the 1st half of the bit.
                    txPacket <<= 1;
                    if ((txPacket & 0x80) != 0)
                    {
                        // We need to send a '1'
                        dalihw_setDALILineLow();
                    }
                    else
                    {
                        // We need to send a '0'
                        dalihw_setDALILineHigh();
                    }
                }
                else
                {
                    // halfBitNumber is odd, 2nd half of the bit.
                    if ((txPacket & 0x80) != 0)
                    {
                        // We're currently sending a '1'
                        dalihw_setDALILineHigh();
                    }
                    else
                    {
                        // We're currently sending a '0'
                        dalihw_setDALILineLow();
                    }
                }
                break;
        }
        // Move on in state ST_SEND_BACKWARD_DATA, by incrementing the halfbit
        // we're transmitting
        halfBitNumber++;
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA)
    {
        // Time-out. Check first if we're at a final bit (got 16 bits)
        // and the last bit is a 1 or a 0. We need to add an extra delay
        // of 1 Te if this is the final bit of a packet and the last bit was
        // a 1, just to make sure that we exit ST_RECEIVE_FORWARD_DATA after
        // the last stop bit has been fully received
        if ((rxPacketLen == 16) && ((rxPacket & 0x01) != 0x00))
        {
            // The last bit we've received is very likely the last one in the
            // transmission, and it is a '1'. Thus, we need to add a 1 Te
            // delay
            // keep the Te timer on, reload timer with 1 Te
            daliProtocolState = ST_RECEIVE_FWD_DATA_EXTRA_TE;
            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
        }
        else if ((rxPacketLen == 16) && ((halfBitNumber == 2) || (halfBitNumber == 4)))
        {
            // What we've just got is a forward frame, as it has 16 bits.
            // We've made sure we haven't received any stray transitions
            // during or after the last bit, by checking the halfBitNumber
            // sub-state
            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx7_VALUE);
            daliProtocolState = ST_WAIT_FOR_SEND;
            // Call idali_receiveForwardFrame after state has been set to
            // ST_WAIT_FOR_SEND such that if this function subsequently calls
            // idali_sendBackwardFrame() the state check will pass and load the
            // value for transmission
//            printf("rp\n");
            idali_receiveForwardFrame(rxPacket);
        }
        else
        {
            // ERROR: received wrong number of bits. Abort.
            // Go directly to ST_IDLE instead of going to
            // ST_WAIT_AFTER_CODE_VIOLATION since this timeout signalled that
            // we've already had 4 Te with no bus activity.
            dalihw_teTimerOff();
            daliProtocolState = ST_IDLE;
        }
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA_EXTRA_TE)
    {
        // Time-out. If we didn't have any transition during this last wait
        // (which is safe to assume as such a transition would have stopped
        // the transmission and signalled an error already) we can now check
        // the data received.
        if ((rxPacketLen == 16) && ((halfBitNumber == 2) || (halfBitNumber == 4)))
        {
            // What we've just got is a forward frame, as it has 16 bits.
            // We've made sure we haven't received any stray transitions
            // during or after the last bit, by checking the halfBitNumber
            // sub-state
            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx7_VALUE);
            daliProtocolState = ST_WAIT_FOR_SEND;
            // Call idali_receiveForwardFrame after state has been set to
            // ST_WAIT_FOR_SEND such that if this function subsequently calls
            // idali_sendBackwardFrame() the state check will pass and load the
            // value for transmission
            idali_receiveForwardFrame(rxPacket);
        }
        else
        {
            // ERROR: received wrong number of bits. Abort.
            // Go directly to ST_IDLE instead of going to
            // ST_WAIT_AFTER_CODE_VIOLATION since this timeout signalled that
            // we've already had 4 Te with no bus activity.
            dalihw_teTimerOff();
            daliProtocolState = ST_IDLE;
        }

    }
    else if (daliProtocolState == ST_WAIT_FOR_SEND)
    {
        // During ST_WAIT_FOR_SEND transmission is not allowed, but we can
        // load a packet for transmission.
        // This time-out means that this state has finished and sending is now
        // allowed. We can either start sending a packet that was already ready,
        // or wait some more for a possible packet from the upper layers.
        if (daliProtocolFlags.backwardFrameToSend == 1)
        {
            // Transmit frame, i.e. move to the TX state and start putting
            // bits on the line
            daliProtocolFlags.backwardFrameToSend = 0;
            daliProtocolState = ST_SEND_BACKWARD_DATA;
            halfBitNumber = 1;
            txPacket = backwardFrame;
            dalihw_teTimerSetVal(TE_VALUE);
            dalihw_setDALILineLow();
        }
        else
        {
            // No frame ready for transmission, we can move to the next state
            // which waits for one. This state will last 22 - 7 = 15 Te at most
            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx15_VALUE);
            daliProtocolState = ST_CAN_SEND;
        }
    }
    else if (daliProtocolState == ST_CAN_SEND)
    {
        // During ST_CAN_SEND we can immediately begin transmitting a backward
        // frame.
        // However, since a time-out occured, we will now move to ST_IDLE
        dalihw_teTimerOff();
        daliProtocolState = ST_IDLE;
    }
    else if (daliProtocolState == ST_WAIT_AFTER_CODE_VIOLATION)
    {
        // This state is used as a safeguard after a code violation has been
        // detected. The standard requires the Control Gear to be ready to
        // receive a new forward frame 1.7ms (4 Te) after a code violation has
        // been detected.
//        printf("V\n");
        dalihw_teTimerOff();
        daliProtocolState = ST_IDLE;
//        printf("\n");
    }

}


void dali_interruptExternal(void)
{
    static uint16_t intValue;
    int8_t daliLow;

    // DALI bus level changed. Based on the state we're currently in, this
    // transition needs to be interpreted.
    if (daliProtocolState == ST_IDLE)
    {
        // Something unexpected happened on the bus.

        if (dalihw_isDALILineLow())
        {
//            printf("S%u",dalihw_isDALILineLow());
            
            // A falling edge has been detected. Either this is a frame coming,
            // or the cable has been disconnected. The cable is being polled
            // every millisecond by the DALI state machine, so we need to assume
            // that a packet is going to arrive.
            rxPacket = 0;
            rxPacketLen = 0;
            halfBitNumber = 0;
            dalihw_teTimerSetVal(TEx4_VALUE);
            daliProtocolState = ST_RECEIVE_FWD_DATA;
            dalihw_teTimerOn();
        }
        // Note that we don't care if we got here by a rising edge event, since
        // this should normally only happen after the cable has just been
        // connected.

        // Just as a precaution, we should make sure we're not driving our own
        // line low.
        dalihw_setDALILineHigh();
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA)
    {
        intValue = dalihw_teTimerGetVal();
        daliLow = dalihw_isDALILineLow();
        dalihw_teTimerSetVal(TEx4_VALUE);
        switch (halfBitNumber)
        {
            case 0:
//                printf("0");
                // halfBitNumber == 0 means "this should be the middle of the
                // start bit". Thus a Te now means we've got it ok, while
                // anything else means an error has occured.
//                printf("s\n");
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
//                    printf("p0%u %u\n", intValue, daliLow);
//                    printf("p");
                    halfBitNumber = 2;
                }
                else
                {
//                    printf("V0%u %u\n", intValue, daliLow);
//                    printf("0");
//                    printf("v0");
                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 1:
//                printf("1");
                // halfBitNumber == 1 means "we were in the first part of
                // receiving a 1". Thus a Te now means we've got the 1 ok, while
                // anything else means an error has occured.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // Middle of a '1' bit
                    rxPacket <<= 1;
                    rxPacket |= 1;
                    rxPacketLen++;
                    halfBitNumber = 2;
//                    printf("p1%u %u\n", intValue, daliLow);
                }
                else
                {
//                    printf("v1");
                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
                
            case 2:
//                printf("2");
                // halfBitNumber == 2 means "we were in the middle of receiving
                // a 1". Thus a Te now means we're getting another 1 and this is
                // the beginning, a 2 Te means we're getting a 0 and this is
                // the middle.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '1' bit
                    halfBitNumber = 1;
                }
                else if ((intValue >= TEx2_MIN) && (intValue <= TEx2_MAX))
                {
//                    printf("2T%u", daliLow);
                    // Middle of a '0' bit
                    rxPacket <<= 1;
                    rxPacketLen++;
                    halfBitNumber = 3;
                }
                else
                {
//                    printf("v2");
                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 3:
//                printf("3");
                // halfBitNumber == 3 means "we were in the middle of receiving
                // a 0". Thus a Te now means we're getting another 0 and this is
                // the beginning, a 2 Te means we're getting a 1 and this is
                // the middle.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '0' bit
                    halfBitNumber = 4;
                }
                else if ((intValue >= TEx2_MIN) && (intValue <= TEx2_MAX))
                {
                    // Middle of a '1' bit
                    rxPacket <<= 1;
                    rxPacket |= 1;
                    rxPacketLen++;
                    halfBitNumber = 2;
                }
                else
                {
//                    printf("v3");
                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 4:
//                printf("4");
                // halfBitNumber == 4 means "we were in the first part of
                // receiving a 0". Thus a Te now means we've got the 0 ok, while
                // anything else means an error has occured.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '0' bit received OK
                    rxPacket <<= 1;
                    rxPacketLen++;
                    halfBitNumber = 3;
                }
                else
                {
//                    printf("v4");
                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;

        }
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA_EXTRA_TE)
    {
        // Transition during the last half of the 2nd stop bit, which is
        // considered an error. Abort reception.
        dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx4_VALUE);
        daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
    }
//if (daliProtocolState == ST_WAIT_AFTER_CODE_VIOLATION)
//    {
//     printf("Vi%u\n", intValue);
//    }
    // Arm the interrupt such that it will fire on the opposite edge as well.
//    printf("e\n");
    dalihw_extInterruptToggleEdge();
}


void idali_sendBackwardFrame(uint8_t data)
{
//    printf("B");
    // As the timer interrupt may occur after the state check is made and move
    // us away from the state we thought we were in, we disable all interrupts
    // (which is not a problem given the limited length of this function).
    // One could also stop the timer but this would introduce a slight delay in
    // the duration of the ST_WAIT_FOR_SEND state.

//    di();
    // If we are not in one of these two states, we can't send the packet any
    // more, since it came too late.
    if (daliProtocolState == ST_WAIT_FOR_SEND)
    {
        // We are waiting for the time for sending backframes to come.
        daliProtocolFlags.backwardFrameToSend = 1;
        backwardFrame = data;
    }
    else if (daliProtocolState == ST_CAN_SEND)
    {
        // Start transmitting frame, i.e. move to the TX state and start putting
        // bits on the line.
        daliProtocolState = ST_SEND_BACKWARD_DATA;
        halfBitNumber = 1;
        txPacket = data;
        dalihw_teTimerSetVal(TE_VALUE);
        dalihw_setDALILineLow();
        // Given that the timer still runs, it may have overflowed until we got
        // to reset its TE_TIMER_VAL. Thus, we clear its flag just to make sure,
        // since this flag set at this point would ruin our start bit.
        dalihw_teTimerClearInterrupt();
    }
//    ei();
}

/**********************Function implementations*(END)**************************/
