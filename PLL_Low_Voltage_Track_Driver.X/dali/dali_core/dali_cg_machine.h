/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_machine.h
 *
 * About:
 *  Internal definitions for the DALI machine.
 *
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Stephen Bowling   19 February 2008
 * Shaima Husain     29 February 2012
 * Mihai Cuciuc      18 October 2013     Implemented the DALI library as a set
 *                                       of multiple loosely coupled layers.
 ******************************************************************************/

#ifndef DALI_CG_MACHINE_H
#define	DALI_CG_MACHINE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "../dali_cg.h"


// The DALI machine needs to save its variables to non-volatile memory and will
// need this many bytes. The nvmem_daliWriteByte() and nvmem_daliReadByte()
// functions will access locations from 0 to DALI_MACHINE_NVMEMORY_REQUIRED - 1.
// How the application implements these non-volatile memory locations is
// transparent to the DALI state machine.
#ifdef DALI_USE_DEVICE_TYPE_6
#define DALI_MACHINE_NVMEMORY_REQUIRED          31
#else
#define DALI_MACHINE_NVMEMORY_REQUIRED          28
#endif /* DALI_USE_DEVICE_TYPE_6 */


// This block of NVMEMORY_DEFAULT_* symbols is used to tell the application
// layer the default values that should be placed in the
// DALI_MACHINE_NVMEMORY_REQUIRED non-volatile memory locations when programming
// the device, as these are the values the device needs to leave the factory
// with. This order should be preserved, as it corresponds to the NVMEMORY_*
// order defined in dali_cg_machine.c There should be no need to add such
// variables unless the standard changes, any non-volatile memory used by the
// application can be defined and used in dali_cg_nvmemory.c.
#define NVMEMORY_DEFAULT_POWER_ON_LEVEL         254
#define NVMEMORY_DEFAULT_SYSTEM_FAILURE_LEVEL   254
#define NVMEMORY_DEFAULT_MINIMUM_LEVEL          PHYSICAL_MINIMUM_LEVEL
#define NVMEMORY_DEFAULT_MAXIMUM_LEVEL          254
#define NVMEMORY_DEFAULT_FADE_RATE              7
#define NVMEMORY_DEFAULT_FADE_TIME              0
#define NVMEMORY_DEFAULT_SHORT_ADDRESS          255
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_H       0xFF
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_M       0xFF
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_L       0xFF
#define NVMEMORY_DEFAULT_GROUP_LOW              0
#define NVMEMORY_DEFAULT_GROUP_HIGH             0
#define NVMEMORY_DEFAULT_SCENES                 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255

#ifdef DALI_USE_DEVICE_TYPE_6
// And LED specific ones
#define NVMEMORY_DEFAULT_FADE_TIME              0
#define NVMEMORY_DEFAULT_DIMMING_CURVE          0
#define NVMEMORY_DEFAULT_MULTIPLE_BITS          0x01
#endif /* DALI_USE_DEVICE_TYPE_6 */


// The nvmem_daliBankMemoryRead() function may signal errors using the
// following return value:
#define BANK_MEMORY_READ_ERROR                  0x0100


#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// The nvmem_daliBankMemoryWrite() function should return its completion
// status using the following values:
#define BANK_MEMORY_WRITE_OK_NOT_LAST_LOC       0x00
#define BANK_MEMORY_WRITE_OK_LAST_LOC           0x01
#define BANK_MEMORY_WRITE_CANNOT_WRITE          0x02
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


/** \brief Initialise DALI state machine from power-up.
 *
 * It calls the DALI protocol initialiser and configures the machine with the
 * values read from non-volatile memory. This function is called by dali_init().
 */
inline void idali_initMachine();


/** \brief DALI State machine 1ms tick.
 *
 * This function is used to derive the different software timers of the DALI
 * machine. It also runs the fading mechanism and the detection of the DALI
 * cable disconnection.
 */
inline void idali_tick1msMachine();


/** \brief This function is called by the DALI protocol layer upon reception of
 * a valid forward frame.
 *
 * Since it is called from the interrupt service routine, it justs copies the
 * data into a variable and sets a bit to signal that a frame has been received
 * for the state machine (which runs from the mainline code) to be able to pick
 * up and process the frame.
 *
 * @param data DALI forward frame payload that has been received.
 */
void idali_receiveForwardFrame(uint16_t data);



#ifdef	__cplusplus
}
#endif

#endif	/* DALI_CG_MACHINE_H */

