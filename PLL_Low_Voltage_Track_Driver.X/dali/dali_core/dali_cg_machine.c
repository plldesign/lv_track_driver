/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_machine.c
 *
 * About:
 *  Core of the DALI protocol implementation for LED devices. The aim of this
 *  file is to implement a hardware-independent subsystem that complies with
 *    IEC 62386-102 (DALI General requirements - Control gear)
 *    IEC 62386-207 (DALI Particular requirements for control gear - LED modules
 *                   (device type 6))
 *  It uses dali_cg_protocol.c/h to communicate on the DALI bus.
 *
 * Limitations:
 *  - The DALI standard dictates the minimum size and the structure of memory
 *    bank 0. It also states that if memory bank 0 is larger than the minimum
 *    size, its writability may be defined by the control gear manufacturer.
 *    While this stack can accomodate changing the size of memory bank 0, this
 *    bank will be implemented as read-only. This limitation only applies to
 *    memory bank 0.
 *  - The DALI standard defines a maximum of 256 memory banks and a minimum of 1.
 *    This stack can accomodate up to 8 memory banks, with more banks being
 *    accomodated with minor modifications.
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Stephen Bowling   19 February 2008
 * Shaima Husain     29 February 2012
 * Mihai Cuciuc      18 October 2013     Implemented the DALI library as a set
 *                                       of multiple loosely coupled layers.
 ******************************************************************************/


#include "dali_cg_machine.h"

#include "../dali_cg.h"

#include "dali_cg_layer.h"
#include "dali_cg_protocol.h"
#include "../dali_cg_nvmemory.h"
#include "../dali_cg_hardware.h"

#include "../../random.h"
#include "../../fader.h"

/**********************DALI machine definitions********************************/

#ifdef DALI_USE_DEVICE_TYPE_6
// Device type, according to DALI standard. This file implements the LED command
// set of the DALI system as well as the basic commands. This makes it comply
// with DALI Device Type 6.
#define DEVICE_TYPE                                 0x06
#else
// If the library doesn't support the LED command set, we still need to respond
// to the device type query. Value 255 tells the control device that multiple
// device types are supported.
#define DEVICE_TYPE                                 255
#endif /* DALI_USE_DEVICE_TYPE_6 */


// The lamp can either be commanded to go to a new power level directly, or
// using either fade time or fade rate. These are being given as parameters to
// the idali_setArcPower() function such that it can properly configure the lamp
// power and the fading mechanism.
#define FADE_NONE                                   0x00
#define FADE_FADE_TIME                              0x01
#define FADE_FADE_RATE                              0x02


// The DALI state machine uses a set of flags to store its state. They are
// defined here.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned fadeUp                             : 1;    // Flag telling the fader that it should fade up
        unsigned withdrawFromCompare                : 1;    // During initialisation, we may be told to withdraw from the comparison process
        unsigned physicalSelectionEnabled           : 1;    // The Control Device may instruct us to use physical selection
        unsigned receivedPacket                     : 1;    // A packet has been received and awaits processing
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
        unsigned writeEnabled                       : 1;    // The memory is write enabled
        unsigned writeEnabledOld                    : 1;    // Previous state of write enable state, used to form a latch
#else
        unsigned                                    : 2;    // Unused bits if bank memory writing is not implemented
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
#ifdef DALI_USE_DEVICE_TYPE_6
        unsigned enabledDeviceType                  : 1;    // Device Type 6 specific commands need to be enabled first. This flag indicates that such enabling has taken place
        unsigned enabledDeviceTypeOld               : 1;    // Previous state of device type enable, used to form a latch
#else
        unsigned                                    : 2;    // Unused bits if Device Type 6 is not enabled
#endif /* DALI_USE_DEVICE_TYPE_6 */
    };
}tdali_flags_state_machine;


#ifdef DALI_USE_DEVICE_TYPE_6
// LED specific machine flags
typedef union
{
    uint8_t All;
    struct
    {
        unsigned currentProtectorEnabled            : 1;    // The current protector state
        unsigned                                    : 7;    // Unused bits
    };
}tdali_flags_state_machine_led;
#endif /* DALI_USE_DEVICE_TYPE_6 */


// When processing a command, various addressing modes are being stored as flags
// in a variable that uses this structure.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned deviceAddressed                    : 1;    // The device is being addressed
        unsigned publicAddress                      : 1;    // Device is addressed using a "public" address
        unsigned individualAddress                  : 1;    // Device is addressed using an individual address
        unsigned commandRepeated                    : 1;    // Command has been received a second time within 100ms
        unsigned                                    : 4;    // Unused locations
    };
}tdali_flags_command;


// The DALI protocol defines these bits for its status.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned controlGearErr                     : 1;    // Status of control gear. "0" = OK
        unsigned lampFailure                        : 1;    // "0" = OK
        unsigned lampArcPowerOn                     : 1;    // "0" = OFF
        unsigned limitError                         : 1;    // "0" = Last requested arc power level is between MIN and MAX LEVEL or OFF
        unsigned fadeRunning                        : 1;    // "0" = Fade is ready; "1" = fade is running
        unsigned resetState                         : 1;    // "0" = No
        unsigned missingShortAddress                : 1;    // "0" = No
        unsigned powerFailure                       : 1;    // "0" = No. "RESET" or an arc power control command has been received since last power-on
    };
}tdali_flags_status;

#ifdef DALI_USE_DEVICE_TYPE_6


// The LED specifications for the DALI protocol defines these failure status
// flags.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned shortCircuit                       : 1;    // "0" = No
        unsigned openCircuit                        : 1;    // "0" = No
        unsigned loadDecrease                       : 1;    // "0" = No
        unsigned loadIncrease                       : 1;    // "0" = No
        unsigned currentProtectorActive             : 1;    // "0" = No
        unsigned thermalShutDown                    : 1;    // "0" = No
        unsigned thermalOverload                    : 1;    // "0" = No
        unsigned referenceMeasurementFailed         : 1;    // "0" = No
    };
}tdali_flags_failure_status;


// The LED specifications for the DALI protocol defines this structure for the
// operating mode.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned pwm                                : 1;    // "0" = No
        unsigned am                                 : 1;    // "0" = No
        unsigned outputCurrentControlled            : 1;    // "0" = No
        unsigned highCurrentPulseMode               : 1;    // "0" = No
        unsigned nonLogDimmingCurve                 : 1;    // "0" = No
        unsigned                                    : 3;    // Unused locations
    };
}tdali_flags_led_operation_mode;

#endif /* DALI_USE_DEVICE_TYPE_6 */


// The fader uses a 16bit counter with overflow/underflow detection. It also
// needs access to its individual bytes as well. The structure of this counter
// is as follows:
// byte[3] byte[2] byte[1] byte[0]
//   |       |       |       |
//   |       |       |       \-- Lower 8 bits of the 16bit timer, being
//   |       |       |           interpreted as the fractional part of the power
//   |       |       |           level. It is required since some fades need to
//   |       |       |           run much slower than 1 power level increment
//   |       |       |           every 5ms
//   |       |       |
//   |       |       \----- Higher 8 bits of the 16bit timer. This is exactly
//   |       |              the power level that needs to be sent to the lamp
//   |       |
//   |       \-------- This should be 0 at all times, since we are using a 16bit
//   |                 counter. It is used to detect overflow/underflow, since
//   |                 if the counter holds the value 0x0000FFF2 and we add
//   |                 0x00000020 we get 0x00010012. Now this value is non-zero
//   |                 and we know we had an overflow.
//   |                 Similarly when fading down, going from 0x00000003 and
//   |                 subtracting 0x00000012 we get 0xFFFFFFF1. Again, this
//   |                 group being non-zero we can detect an underflow.
//   |
//   \----------- Unused.
typedef union
{
    uint32_t counter;
    struct
    {
        uint8_t byte[4];
    };
}tdali_fade_counter;

/**********************DALI Machine definitions*(END)**************************/



/**********************DALI Machine variables**********************************/

/** \brief Array which aids in the decoding of the groups.
 *
 * Using this method gives better results than using "1<<i" does.
 */
static const uint8_t groupMask[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};


/** \brief Fade time table, as defined by the DALI standard.
 *
 * The fader is run every 5ms, thus these values represent the number of fader
 * steps that make up the given fade time. (0s, 0.7s, 1s,...)
 */
static const uint16_t fadeTimeTable[16] =
{
    0,140,200,280,400,560,800,1040,1600,
    2260,3200,4520,6400,9060,12800,18100
};


/** \brief Fade rate table, as defined by the DALI standard.
 *
 * The fader uses a 16bit counter of which the high 8 bits are the lamp power
 * level, and the low 8 bits are used to increase dimming accuracy. As such,
 * these values are used to make the high 8 bits change at the fade rate
 * requested by the standard. The values from the table in IEC 62386-102 are
 * multiplied by 256 (shift left 8 bits) and divided by 200 (fader frequency,
 * its period being 5ms).
 */
static const uint16_t fadeRateTable[16] =
{
    648,458,324,229,162,114,
    81,57,40,29,20,14,10,7,5,3
};


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Fast fade time table, as defined by the DALI standard for Device Type
 * 6 devices.
 *
 * The fader is run every 5ms, thus these values represent the number of fader
 * steps that make up the given fast fade time. (0ms, 25ms, 50ms,...)
 */
static const uint8_t fastFadeTimeTable[28] =
{
    0,5,10,15,20,25,30,35,
    40,45,50,55,60,65,70,75,
    80,85,90,95,100,105,110,115,
    120,125,130,135
};

#endif /* DALI_USE_DEVICE_TYPE_6 */


// Non-Volatile Memory data. These values are the locations that the
// NVMemoryDALIReadByte() and NVMemoryDALIWriteByte() will read from/write to.
// Default values are provided in dali_cg_machine.h since
// those are the ones the control gear should leave the factory with.
#define NVMEMORY_POWER_ON_LEVEL          0
#define NVMEMORY_SYSTEM_FAILURE_LEVEL    1
#define NVMEMORY_MINIMUM_LEVEL           2
#define NVMEMORY_MAXIMUM_LEVEL           3
#define NVMEMORY_FADE_RATE               4
#define NVMEMORY_FADE_TIME               5
#define NVMEMORY_SHORT_ADDRESS           6
#define NVMEMORY_RANDOM_ADDRESS_H        7
#define NVMEMORY_RANDOM_ADDRESS_M        8
#define NVMEMORY_RANDOM_ADDRESS_L        9
#define NVMEMORY_GROUP_LOW              10
#define NVMEMORY_GROUP_HIGH             11
#define NVMEMORY_SCENES_BASE            12  // 16 values

#ifdef DALI_USE_DEVICE_TYPE_6
// And LED specific ones
#define NVMEMORY_FAST_FADE_TIME         NVMEMORY_SCENES_BASE + 16
#define NVMEMORY_DIMMING_CURVE          NVMEMORY_SCENES_BASE + 17
#define NVMEMORY_MULTIPLE_BITS          NVMEMORY_SCENES_BASE + 18


// The NVMEMORY_MULTIPLE_BITS location contains 3 meaningful bits:
//   bit 7: reference measurement failed
//   bit 4: linear dimming curve selected
//   bit 0: current protector enabled state
// We define masks for each of these
#define NVMEMORY_MULTIPLE_BITS_REF_FAIL 0x80
#define NVMEMORY_MULTIPLE_BITS_LIN_DIM  0x08
#define NVMEMORY_MULTIPLE_BITS_CP_EN    0x01
#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Power on level (shadow of Non-Volatile Memory value). */
static uint8_t powerOnLevel;


/** \brief System failure level (shadow of Non-Volatile Memory value). */
static uint8_t systemFailureLevel;


/** \brief Minimum level (shadow of Non-Volatile Memory value). */
static uint8_t minLevel;


/** \brief Maximum level (shadow of Non-Volatile Memory value). */
static uint8_t maxLevel;


/** \brief Fade rate (shadow of Non-Volatile Memory value). */
static uint8_t fadeRate;


/** \brief Fade time (shadow of Non-Volatile Memory value). */
static uint8_t fadeTime;


/** \brief Short address (shadow of Non-Volatile Memory value).
 *
 * In 0AAA AAA1 format.
 */
static uint8_t shortAddress;


/** \brief Random address high byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressH;


/** \brief Random address medium byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressM;


/** \brief Random address low byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressL;


/** \brief Group low byte (shadow of Non-Volatile Memory value). */
static uint8_t groupLow;


/** \brief Group high byte (shadow of Non-Volatile Memory value). */
static uint8_t groupHigh;


/** \brief Scene levels (shadow of Non-Volatile Memory value). */
static uint8_t scenes[16];


/** \brief Last arc power level.
 *
 * Used during power-up, if the device needs to restore the last used power
 * level.
 */
static uint8_t lastArcValue;


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Fast fade time (shadow of Non-Volatile Memory value). */
static uint8_t fastFadeTime;

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Actual arc power level. */
static volatile uint8_t actualArcPower;


/** \brief Search address high byte. */
static uint8_t searchAddressH;


/** \brief Search address medium byte. */
static uint8_t searchAddressM;


/** \brief Search address low byte. */
static uint8_t searchAddressL;


/** \brief Data transfer register. */
static uint8_t DTR;


/** \brief Data transfer register 1. */
static uint8_t DTR1;


/** \brief Data transfer register 2. */
static uint8_t DTR2;


/** \brief DALI status flags. */
static volatile tdali_flags_status status;


#ifdef DALI_USE_DEVICE_TYPE_6

// LED specific DALI variables

/** \brief Physical minimum level.
 *
 * When implementing a DALI Device Type 6 (LED) device, the physical minimum
 * level needs to be adjusted according to the selected dimming curve (linear or
 * logarithmic).
 */
static uint8_t physicalMinimumLevel;


/** \brief DALI failure status flags. */
static tdali_flags_failure_status failureStatus;


/** \brief DALI operating mode. */
static tdali_flags_led_operation_mode operatingMode;


/** \brief DALI variable which keeps the enable status of the current
 * protector.
 */
static tdali_flags_state_machine_led daliLedStateFlags;


#else

#define physicalMinimumLevel        PHYSICAL_MINIMUM_LEVEL

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Command time-out variable.
 *
 * Value measured in milliseconds. Used for commands that need to be received
 * twice within 100ms to be executed.
 */
static volatile uint8_t commandTimeout_ms = 0;


/** \brief DAPC sequence time-out variable.
 *
 * Value measured in milliseconds. If no DAPC command is received within 200ms,
 * the sequence needs to be aborted.
 */
static volatile uint8_t dapcTimeout_ms = 0;


/** \brief Power-up time-out variable.
 *
 * Value measured in milliseconds. The device should wait 0.6s after powerup
 * before sending the lamp to the power on level.
 */
static volatile uint8_t powerUpTimeout_100ms = 6;


/** \brief Initialise time-out variable.
 *
 * Value measured in tenths of a second. After receiving an initialise command,
 * this timer will run for 15 minutes or until a terminate command is received.
 */
static volatile uint16_t initialiseTimeout_100ms = 0;


/** \brief Cable disconnection time-out variable.
 *
 * Value measured in milliseconds. If the cable is disconnected for 500ms, the
 * device should react by going to system failure level.
 */
static volatile uint16_t cableDisconnectTimeout_ms = 500;


/** \brief Last command received (high byte).
 * 
 * The command processing mechanism needs to know what the last command was in
 * order to be able to tell if the command has been received twice.
 */
static uint8_t lastAddress;


/** \brief Last command received (low byte).
 * 
 * The command processing mechanism needs to know what the last command was in
 * order to be able to tell if the command has been received twice.
 */
static uint8_t lastCommand;


/** \brief The DALI state machine stores its state in these flags. */
static tdali_flags_state_machine daliStateFlags;


/** \brief Fade counter variable. */
static volatile tdali_fade_counter fadeAccumulator;


/** \brief Value that should be added to/subtracted from the counter every
 * 5ms. */
static uint16_t fadeDelta;


/** \brief Number of 5ms steps that the fader should run. */
static volatile uint16_t fadeCount;


/** \brief Target level for the fading mechanism.
 * 
 * If the target level is known (which it is, unless we're using fade rate),
 * store the target level in this variable. The fader may or may not hit this
 * exact level since there is limited precision. At the very last step of the
 * fader, this value is given to the lamp, thus ending up at the exact value
 * the Control Device asked us to go to.
 */
static uint8_t fadeTimeTargetLevel;


/** \brief Data received from DALI bus.
 * 
 * If the DALI protocol level detects a forward frame, it will call
 * idali_receiveForwardFrame() which will set this variable. Being set from the
 * ISR, it needs to be declared volatile.
 */
static volatile uint16_t daliData;


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

/** \brief Memory bank checksum change flags.
 * 
 * Each bit from this variable corresponds to a memory bank, limiting the number
 * of memory banks to 8. Making this variable wider would increase the number of
 * allowable memory banks. The machine sets the corresponding bit if it writes
 * to the memory bank, and during dali_tasks() these values are checked and
 * the checksum is recomputed for the required banks.
 */
static uint8_t checksumChange;

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

/**********************DALI Machine variables*(END)****************************/



/**********************Local function definitions******************************/


/** \brief Configure the lamp power level and the fader.
 *
 * This function checks whether the level is within the allowable range and sets
 * the limit error bit if it is needed. It computes the fader parameters and
 * starts it, if fading is required.
 *
 * @param level
 *  - if fade == FADE_NONE, go directly to this level (applying the
 *    minLevel/maxLevel bounds) \n
 *  - if fade == FADE_FADE_TIME, set the fader to run for as many steps as
 *    needed such that the lamp will be at this level after the selected fade
 *    time has elapsed \n
 *  - if fade == FADE_FADE_RATE, set the fader to run for 200ms. If level == 0
 *    fade down, otherwise fade up
 * @param fade Type of fade, one of FADE_NONE, FADE_FADE_TIME or FADE_FADE_RATE.
 * The meaning of the "level" parameter depends on this value
 */
static void idali_setArcPower(uint8_t level, uint8_t fade);


/** \brief Set a new short address.
 *
 * The function checks the parameter for the correct format and if it is ok,
 * sets it as the new short address. If it differs from the previous one,
 * updates the non-volatile memory as well. Updates the missing short address
 * bit. Can remove the short address when given the parameter 255.
 *
 * @param newAddress New address that should be set.
 */
static void idali_setShortAddress(uint8_t newAddress);


/** \brief Check if the given parameter represents a group that this Control
 * Gear belongs to.
 *
 * The address should be a valid group address, of the form 100x xxxS. Otherwise
 * (we don't belong to the group or the address is not a group address) this
 * function returns 0.
 *
 * @param address Address that should be checked.
 * @return
 *  - 1 if we belong to the group specified by the "address" parameter.\n
 *  - 0 otherwise (either wrong address type or we don't belong to the specified
 *    group)
 */
static inline uint8_t idali_doesGroupMatch(uint8_t address);


/** \brief Compute and return the checksum for the memory bank.
 *
 * The checksum is computed according to the DALI specifications on memory bank
 * checksums.
 *
 * @param bank Bank for which the checksum should be computed.
 * @return Computed checksum.
 */
static uint8_t idali_computeMembankChecksum(uint8_t bank);


/** \brief Perform the DALI "RESET" command.
 *
 * This reinitialises most of the variables to known (reset) values.
 */
static void idali_reset();


/** \brief Run the DALI fading mechanism.
 *
 * This function is called every 5ms to update the lamp power in a continuous
 * fashion, according to the fade mechanism requested.
 */
//static inline void idali_fade();


/** \brief DALI command decoder.
 *
 * If a DALI forward frame has been received by the protocol layer this function
 * is called by the mainline code to process it. This implements most of the
 * DALI subsystem, it processes every DALI forward frame, checks if the command
 * is addressed to this device and executes it if all conditions are met (some
 * commands require being sent twice, others need an activation command to
 * precede them).
 *
 * @param address First byte of DALI forward frame payload (address byte).
 * @param command Second byte of DALI forward frame payload (data byte).
 */
static void idali_processCommand(uint8_t address, uint8_t command);

/**********************Local function definitions*(END)************************/



/**********************Function implementations********************************/


inline void idali_initMachine()
{
    uint8_t i;

    // Call the DALI protocol initialiser
    idali_initProtocol();

    // Load last lamp power level
    lastArcValue = nvmem_daliReadLastLevel();

#ifdef DALI_USE_DEVICE_TYPE_6
    // Clear LED specific flags
    failureStatus.All = 0;
    operatingMode.All = 0;
    // Load bits from non-volatile memory
    i = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
    if ((i & NVMEMORY_MULTIPLE_BITS_CP_EN) != 0)
    {
        daliLedStateFlags.currentProtectorEnabled = 1;
    }
    if ((i & NVMEMORY_MULTIPLE_BITS_LIN_DIM) != 0)
    {
        operatingMode.nonLogDimmingCurve = 1;
    }
    if ((i & NVMEMORY_MULTIPLE_BITS_REF_FAIL) != 0)
    {
        failureStatus.referenceMeasurementFailed = 1;
    }

    // Read fast fade time
    fastFadeTime = nvmem_daliReadByte(NVMEMORY_FAST_FADE_TIME);

    // The physical minimum level needs to be adjusted based on the dimming
    // curve selected, such that the light output at the physical minimum level
    // be the same regardless of dimming curve choice.
    if (operatingMode.nonLogDimmingCurve == 0)
    {
        physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
    }
    else
    {
        physicalMinimumLevel = idali_getLampNonLogPhysicalMinimum();
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */

    // Load all other variables from the non-volatile memory
    powerOnLevel = nvmem_daliReadByte(NVMEMORY_POWER_ON_LEVEL);
    systemFailureLevel = nvmem_daliReadByte(NVMEMORY_SYSTEM_FAILURE_LEVEL);
    minLevel = nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL);
    maxLevel = nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL);
    fadeRate = nvmem_daliReadByte(NVMEMORY_FADE_RATE);
    fadeTime = nvmem_daliReadByte(NVMEMORY_FADE_TIME);
    shortAddress = nvmem_daliReadByte(NVMEMORY_SHORT_ADDRESS);
    randomAddressH = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H);
    randomAddressM = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M);
    randomAddressL = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L);
    groupLow = nvmem_daliReadByte(NVMEMORY_GROUP_LOW);
    groupHigh = nvmem_daliReadByte(NVMEMORY_GROUP_HIGH);

    // Initialise the status byte with the power failure bit, and, if we don't
    // have a short address, the missing short address bit as well.
    if (shortAddress == 255)
    {
        // Power failure AND Missing short address
        status.All = 0xC0;
    }
    else
    {
        // Power failure
        status.All = 0x80;
    }

    // Read scenes from the non-volatile memory.
    for (i = 0; i < 16; i++)
    {
        scenes[i] = nvmem_daliReadByte(NVMEMORY_SCENES_BASE + i);
    }

#ifdef DALI_USE_EXTRA_MEMORY_BANKS
    checksumChange = 0;
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

    daliStateFlags.All = 0;
}


void dali_tasks()
{
    static uint8_t oldArcPower = 0;
    static uint8_t i;

    // Check for new data and if we have some, handle it
    if (daliStateFlags.receivedPacket == 1)
    {
//        printf("R%x\n", daliData);
        idali_processCommand(daliData >> 8, daliData & 0xFF);
        daliStateFlags.receivedPacket = 0;
    }

#ifdef DALI_USE_DEVICE_TYPE_6
    // Start with the failure status cleared, bits will be populated as
    // necessary
    failureStatus.All = 0;
    // Update operating mode
    operatingMode.All &= ~0x0F;
    operatingMode.All |= idali_getLampOperatingMode();

    // Check if we have had a failure in the reference system power measurement.
    if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_FAILED)
    {
        // Reference system power failed. Set corresponding bit
        failureStatus.referenceMeasurementFailed = 1;
        // Also update bit in the non-volatile memory
        i = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
        if ((i & NVMEMORY_MULTIPLE_BITS_REF_FAIL) == 0)
        {
            i |= NVMEMORY_MULTIPLE_BITS_REF_FAIL;
            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, i);
        }
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */
    
    // Check if we are in the reset state, and if so, set the corresponding bit.
    status.resetState = 0;
    if ((actualArcPower == 254) &&
        (powerOnLevel == 254) &&
        (systemFailureLevel == 254) &&
        (minLevel == physicalMinimumLevel) &&
        (maxLevel == 254) &&
        (fadeRate == 7) &&
        (fadeTime == 0) &&
        (searchAddressH == 0xFF) &&
        (searchAddressM == 0xFF) &&
        (searchAddressL == 0xFF) &&
        (randomAddressH == 0xFF) &&
        (randomAddressM == 0xFF) &&
        (randomAddressL == 0xFF) &&
        (groupLow == 0) &&
        (groupHigh == 0))
    {
        status.resetState = 1;
    }

    // If any of the scenes are programmed, this Control Gear is not in the
    // reset state.
    for (i = 0; i < 16; i++)
    {
        if (scenes[i] != 255)
        {
            status.resetState = 0;
        }
    }

    // Initially set lampArcPowerOn bit according to the requested lamp power.
    if (actualArcPower > 0)
    {
        status.lampArcPowerOn = 1;
    }
    else
    {
        status.lampArcPowerOn = 0;
    }

    // Obtain the lamp status from the application layer and set the failure
    // bits accordingly.
    i = idali_getLampStatus();
#ifdef DALI_USE_DEVICE_TYPE_6
    if ((i & LAMP_STATUS_OPEN_CIRCUIT) != 0)
    {
        failureStatus.openCircuit = 1;
    }
    if ((i & LAMP_STATUS_SHORT_CIRCUIT) != 0)
    {
        failureStatus.shortCircuit = 1;
    }
    if ((i & LAMP_STATUS_LOAD_INCREASE) != 0)
    {
        failureStatus.loadIncrease = 1;
    }
    if ((i & LAMP_STATUS_LOAD_DECREASE) != 0)
    {
        failureStatus.loadDecrease = 1;
    }
    if ((i & LAMP_STATUS_THERMAL_OVEROAD) != 0)
    {
        failureStatus.thermalOverload = 1;
    }
    if ((i & LAMP_STATUS_THERMAL_SHUTDOWN) != 0)
    {
        failureStatus.thermalShutDown = 1;
    }

    // Set the lamp failure bit in the status byte if any sort of failure is
    // detected.
    if (i == 0)
    {
        status.lampFailure = 0;
        failureStatus.currentProtectorActive = 0;
    }

    if ((failureStatus.loadIncrease == 1) || (failureStatus.loadDecrease == 1))
    {
        // Current protector can become active, if needed
        if ((daliLedStateFlags.currentProtectorEnabled == 1) && (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUN_OK) && (actualArcPower > 0))
        {
            // If the current protector is enabled and there has been a
            // successful reference system power measurement, the current
            // protector becomes active, unless the lamp power level is 0.
            // This state also should clear the lampPowerOn bit and signal a
            // lamp failure.
            failureStatus.currentProtectorActive = 1;
            status.lampArcPowerOn = 0;
            status.lampFailure = 1;
        }
    }
    
    if ((failureStatus.openCircuit == 1) || (failureStatus.shortCircuit == 1))
    {
        // Open or short circuits are considered lamp failures.
        status.lampFailure = 1;
        status.lampArcPowerOn = 0;
    }

    if (failureStatus.thermalShutDown == 1)
    {
        // During thermal shutdown, this bit should be 0.
        status.lampArcPowerOn = 0;
    }
#else
    if (i == 0)
    {
        status.lampFailure = 0;
    }
    else
    {
        status.lampFailure = 1;
        status.lampArcPowerOn = 0;
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */

    
#ifdef DALI_USE_EXTRA_MEMORY_BANKS

    // If any of the memory banks have been changed, we need to recompute the
    // checksum. Recompute at most 1 per iteration, as these operations may take
    // time to access the non-volatile memory
    for (i = 0; i <= (sizeof(checksumChange) * 8); i++)
    {
        if ((checksumChange & (1<<i)) != 0)
        {
            // Recompute checksum
            nvmem_daliBankMemoryWrite(i, 1, idali_computeMembankChecksum(i));
            // Clear flag and exit loop. If more flags are set, put this
            // operation off for the following cycles
            checksumChange &= ~(1<<i);
            break;
        }
    }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

    // This mechanism of updating the lamp power values is used since
    // idali_setLampPower() will set a flag signalling the application that a
    // new power level needs to be set. This may need to access slow memory to
    // load values from tables or to compute the value on the fly. This method
    // limits these redundant operations.
    if (actualArcPower != oldArcPower)
    {
#ifdef DALI_USE_DEVICE_TYPE_6
        idali_setLampPower(actualArcPower, operatingMode.nonLogDimmingCurve);
#else
        idali_setLampPower(actualArcPower, 0);
#endif /* DALI_USE_DEVICE_TYPE_6 */
        oldArcPower = actualArcPower;
        
        // If powerOnLevel == 255 write to the non-volatile memory the value of
        // the set power level, such that at power up it can be retrieved
        if (powerOnLevel == 255)
        {
            nvmem_daliWriteLastLevel(actualArcPower);
        }
    }
    
}


inline void idali_tick1msMachine()
{
    static uint8_t divisor100 = 0;  // We need to derive a 100ms tick
    static uint8_t divisor5 = 0;    // ..and a 5ms one for fading
    status.fadeRunning = MASTER_FADER.config.status_flags.set_bit.fading;
    // This timer is used by the commands that need to be received twice within
    // 100ms.
    if (commandTimeout_ms != 0)
    {
        commandTimeout_ms--;
    }

    // This timer is used for the DAPC sequence
    if (dapcTimeout_ms != 0)
    {
        dapcTimeout_ms--;
    }

    // Interface failure. See IEC 62386-102, 9.3 (page 21).
    // Check the DALI line and if it's low keep decrementing a counter until it
    // hits 0. When it is 0, we are in the system failure state. If the DALI
    // line is not low, reload this counter to 500ms.
    if (dalihw_isDALILineLow())
    {
        if (cableDisconnectTimeout_ms > 0)
        {
            cableDisconnectTimeout_ms--;
        }
    }
    else
    {
        cableDisconnectTimeout_ms = 500;
    }

    if (cableDisconnectTimeout_ms == 0)
    {
        if (systemFailureLevel != 255)
        {
            idali_setArcPower(systemFailureLevel, FADE_NONE);
        }
    }

    // Divide 1kHz tick by 5 and run fader
    divisor5++;
    if (divisor5 == 5)
    {
        actualArcPower = get_fader_val_dali(&MASTER_FADER);
        divisor5 = 0;
    }

    // Divite 1kHz tick by 100
    divisor100++;
    if (divisor100 == 100)
    {
        // 0.1s tick

        // After the INITIALISE command has been received there is a 15 minute
        // timer that needs to run. This is that timer.
        if (initialiseTimeout_100ms != 0)
        {
            initialiseTimeout_100ms--;
        }

        // See IEC 62386-102, 9.2 (page 21).
        // This timer is reset by commands affecting power level.
        if (powerUpTimeout_100ms != 0)
        {
            powerUpTimeout_100ms--;
            if (powerUpTimeout_100ms == 0)
            {
                // Go to powerOnLevel or if MASK, to the last power level used.
                // Since the divisor needs to be reset at the end of this loop
                // anyway, use divisor100 as a temporary variable to hold the
                // power level we need to go to.
                if (powerOnLevel == 255)
                {
                    // If powerOnLevel == 255 we need to retrieve the last arc
                    // power level used
                    divisor100 = lastArcValue;
                }
                else
                {
                    divisor100 = powerOnLevel;
                }
                // If divisor still == 255, this means that we started with
                // powerOnLevel == 255 (and needed to retrieve the value from
                // the buffer), but the buffer was empty
                if (divisor100 == 255)
                {
                    divisor100 = 254;
                }
                idali_setArcPower(divisor100, FADE_NONE);
                status.powerFailure = 1;
            }
        }

        // We may have used the divisor as a temporary variable inside this if,
        // so we need to set it to 0 at the end of this if block.
        divisor100 = 0;
    }

}


void idali_receiveForwardFrame(uint16_t data)
{
    // If data is in the buffer already and has not been read by the machine,
    // drop the current frame.
    if (daliStateFlags.receivedPacket == 0)
    {
        daliData = data;
        daliStateFlags.receivedPacket = 1;
    }
}


static void idali_setArcPower(uint8_t level, uint8_t fade)
{
    // level == 255 means "stop fading"
    // This function checks MIN and MAX levels and sets the limit error bit if
    // these are violated.

    // Disable the power-up timer. This timer is only run at power-up and runs
    // for 600ms. If SetArcPower is not called within these 600ms, the value
    // stored in the powerOnLevel variable will be the initial power level.
    powerUpTimeout_100ms = 0;

    // Clear power failure bit. This should be set if the Control Gear has not
    // received any RESET or arc power control command since power-on. The RESET
    // command ends up calling this function so it will clear this bit as well.
    status.powerFailure = 0;

    if (fade != FADE_FADE_RATE)
    {
        // Check that the required level is within the acceptable range and if
        // it isn't, bring it within minLevel and maxLevel
        if (level == 0)
        {
            // level == 0 is acceptable either way.
            status.limitError = 0;
        }
        else if (level == 255)
        {
            fader_halt(&MASTER_FADER);
            // 255 means "STOP FADING. See IEC 62386-102, 11.1.1 (page 27).
        }
        else if (level > maxLevel)
        {
            level = maxLevel;
            status.limitError = 1;
        }
        else if (level < minLevel)
        {
            level = minLevel;
            status.limitError = 1;
        }
        else
        {
            status.limitError = 0;
        }

    }
    
    uint16_t level16 = level*257;
    if (254==level){
        level16 = 65535;
    }
    

    // idali_fade() runs from the ISR and can disturb these values. Disabling
    // interrupts for this section protects against this. The interrupts stay
    // disabled for max. 55us (PRO), 70us (Free) on an Enhanced Mid-Range
    // core @ 32MHz at the end of a successful forward frame reception (during
    // the forbidden period after a frame reception).
//    di();

    // Based on the fade settings set the power level or start fading
    if (fade == FADE_NONE)
    {
        // No fading, go straight to the power level requested
        fader_set_16bit(level16, 0, &MASTER_FADER);
    }
    else if (fade == FADE_FADE_RATE)
    { 
        printf("UD\n");

//        // Fade rate is used with commands "UP" and "DOWN". These should dim up
//        // or down 200ms using the selected fade rate. Since we run the fader
//        // every 5ms, 200ms makes 40 steps.
//        fadeCount = 40;
//
//        // Tell the fading loop that we aren't using fade time (there is no
//        // target level defined, just stop wherever we get after 200ms.
//        fadeTimeTargetLevel = 255;
//
//        // When using the fade rate, the level parameter gives the fading
//        // direction: 0 to fade down, 1 to fade up.
        
        uint16_t step = fadeRateTable[fadeRate]*40;//calculate step from current level to new level.
        uint16_t new_UD_val;
        if (level == 0)
        {
            printf("down\n");
            new_UD_val = get_fader_val_16bit(&MASTER_FADER) - step;
            
            if (new_UD_val > get_fader_val_16bit(&MASTER_FADER)){
                new_UD_val = 0;  
            }

            fader_set_16bit(new_UD_val, 200, &MASTER_FADER);
            daliStateFlags.fadeUp = 0;
        }
        else
        {
             printf("up\n");
            new_UD_val = get_fader_val_16bit(&MASTER_FADER) + step;
            
            if (new_UD_val < get_fader_val_16bit(&MASTER_FADER)){
                new_UD_val = 0xFFFF;  
            }

            fader_set_16bit(new_UD_val, 200, &MASTER_FADER);
            daliStateFlags.fadeUp = 1;
        }

        // Load the step delta from the (precomputed) table
//        fadeDelta = fadeRateTable[fadeRate];
//
//        
//////        
//        
//
//        // If the fader is not running, reinitialise the counter. Otherwise
//        // don't do any initialisation. This allows us to use the full precision
//        // of the fader and make a smoother transition.
//        if (status.fadeRunning == 0)
//        {
//            fadeAccumulator.byte[0] = 0;
//            fadeAccumulator.byte[1] = actualArcPower;
//            fadeAccumulator.byte[2] = 0;
//            fadeAccumulator.byte[3] = 0;
//        }
        
        
        
        
        
//        fader_set_16bit_SC(level16, 200, &MASTER_FADER);
    }
    else if (fade == FADE_FADE_TIME)
    {
        
        // Use fade time. If we are in a DAPC sequence, we should use a
        // "special" fade time that is loosely defined in the standard.
        // Otherwise, use either the programmed fade time or the programmed fast
        // fade time.
        if (dapcTimeout_ms == 0)
        {
            if (level == actualArcPower)
            {
                // We're trying to fade to the same level, which is a NOP. This
                // can cause problems in the fader for
                // level == actualArcPower == 0.
                fadeCount = 0;
            }
            else
            {
                // We are not in a DAPC sequence. Load number of steps we should
                // run the fader from the (precomputed) table.
                fadeCount = fadeTimeTable[fadeTime] * 5;

#ifdef DALI_USE_DEVICE_TYPE_6
                // If this value turns out to be 0, we should use the fast fade time
                // defined for LED devices.
                if (fadeCount == 0)
                {
                    // Load number of steps we should run the fader from the
                    // other (precomputed) table.
                    fadeCount = fastFadeTimeTable[fastFadeTime] * 5;
                }
#endif /* DALI_USE_DEVICE_TYPE_6 */

            }
        }
        else
        {
            // We are in a DAPC sequence, use "special" fade time and also
            // reload timer such that the sequence can be as long as the Control
            // Device wants it.
            dapcTimeout_ms = 200;
            fadeCount = DAPC_SEQUENCE_FADE_TIME_MS;
        }
        
        fader_set_16bit_SC(level16,  fadeCount, &MASTER_FADER);
    }
}


static void idali_setShortAddress(uint8_t newAddress)
{
    // This function should be used to update the short address, as it will
    // check for the correct format and will also erase the short address when
    // given 0xFF as a parameter.
    uint8_t oldAddress;
    oldAddress = shortAddress;

    if (newAddress == 0xFF)
    {
        // Delete short address
        shortAddress = 0xFF;
        status.missingShortAddress = 1;
    }
    else if ((newAddress & 0x81) == 0x01)
    {
        // If the address has the right format, i.e. 0xxxxxx1, program
        // short address
        shortAddress = newAddress;
        status.missingShortAddress = 0;
    }

    if (newAddress != oldAddress)
    {
        // If our new address is different from the previously stored one,
        // update the non-volatile memory as well
        nvmem_daliWriteByte(NVMEMORY_SHORT_ADDRESS, shortAddress);
    }
}


static inline uint8_t idali_doesGroupMatch(uint8_t address)
{
    uint8_t val;
    val = 0;
    // Firstly, check if the parameter given has the correct format to be a
    // group address. Return 0 if it doesn't
    if ((address & 0xE0) == 0x80) // address is 100x xxxx
    {
        // Strip off extra bits; val = group number
        val = (address >> 1) & 0x0F;
        if (val < 8)
        {
            // We should check the groupLow variable
            if ((groupLow & groupMask[val]) != 0)
            {
                // We do belong to this group, return 1
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            // We should check the groupHigh variable
            if ((groupHigh & groupMask[val - 8]) != 0)
            {
                // We do belong to this group, return 1
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        return 0;
    }
}


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

static uint8_t idali_computeMembankChecksum(uint8_t bank)
{
    uint8_t chk = 0;
    uint8_t i;
    uint16_t memVal;

    i = 2;
    memVal = nvmem_daliBankMemoryRead(bank, i);
    while ((memVal & 0xFF00) == 0)
    {
        // Keep reading until we hit an error and memVal will have something in
        // the high 8 bits
        chk -= memVal;
        i++;
        memVal = nvmem_daliBankMemoryRead(bank, i);
    }

    return chk;
}

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */



static void idali_reset()
{
    uint8_t i;

    // This function loads the reset values into the variables. If the newly
    // loaded values differ from the ones that were saved to the non-volatile
    // memory, update the non-volatile memory as well.

#ifdef DALI_USE_DEVICE_TYPE_6
    operatingMode.nonLogDimmingCurve = 0;
    physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
    
    fastFadeTime = 0;
    if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != fastFadeTime)
    {
        nvmem_daliWriteByte(NVMEMORY_FADE_TIME, 0);
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */

    if (shortAddress == 255)
    {
        // Reset state AND Missing short address
        status.All = 0x60;
    }
    else
    {
        // Reset state
        status.All = 0x20;
    }

    status.All = 0x20;
    powerOnLevel = 254;
    if (nvmem_daliReadByte(NVMEMORY_POWER_ON_LEVEL) != 254)
    {
        nvmem_daliWriteByte(NVMEMORY_POWER_ON_LEVEL, 254);
    }

    systemFailureLevel = 254;
    if (nvmem_daliReadByte(NVMEMORY_SYSTEM_FAILURE_LEVEL) != 254)
    {
        nvmem_daliWriteByte(NVMEMORY_SYSTEM_FAILURE_LEVEL, 254);
    }

    minLevel = physicalMinimumLevel;
    if (nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL) != physicalMinimumLevel)
    {
        nvmem_daliWriteByte(NVMEMORY_MINIMUM_LEVEL, physicalMinimumLevel);
    }

    maxLevel = 254;
    if (nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL) != 254)
    {
        nvmem_daliWriteByte(NVMEMORY_MAXIMUM_LEVEL, 254);
    }

    fadeRate = 7;
    if (nvmem_daliReadByte(NVMEMORY_FADE_RATE) != 7)
    {
        nvmem_daliWriteByte(NVMEMORY_FADE_RATE, 7);
    }

    fadeTime = 0;
    if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != 0)
    {
        nvmem_daliWriteByte(NVMEMORY_FADE_TIME, 0);
    }

    searchAddressH = 0xFF;
    searchAddressM = 0xFF;
    searchAddressL = 0xFF;

    randomAddressH = 0xFF;
    randomAddressM = 0xFF;
    randomAddressL = 0xFF;
    if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H) != 0xFF)
    {
        nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_H, 0xFF);
    }
    if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M) != 0xFF)
    {
        nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_M, 0xFF);
    }
    if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L) != 0xFF)
    {
        nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_L, 0xFF);
    }

    groupLow = 0;
    if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != 0)
    {
        nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, 0);
    }

    groupHigh = 0;
    if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != 0)
    {
        nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, 0);
    }

    for (i = 0; i < 16; i++)
    {
        scenes[i] = 255;
        if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + i) != 255)
        {
            nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + i, 255);
        }
    }

    idali_setArcPower(254, FADE_NONE);
    daliStateFlags.All = 0;
}




//static inline void idali_fade()
//{
//    // Check if fader should be running. If it shouldn't, clear the fade running
//    // bit and return
//    if (fadeCount != 0)
//    {
//        // Decrement counter and signal that the fader is running
//        fadeCount--;
//        status.fadeRunning = 1;
//
//        if ((fadeCount == 0) && (fadeTimeTargetLevel != 255))
//        {
//            // This is the last step of a fade with fade time. This should bring
//            // us EXACTLY at the requested target level, even if the math that
//            // uses approximations didn't.
//            // fadeTimeTargetLevel is set by idali_setArcPower() to:
//            //   - 255 if we are using the fade rate (where no target level is
//            //     defined, fade rate means we should fade for 200ms and then
//            //     stop wherever it lands us)
//            //   - a target power level (i.e. different from 255) when using any
//            //     form of fade time (fade time, fast fade time or the special
//            //     fade time used in the DAPC sequence)
//            actualArcPower = fadeTimeTargetLevel;
//        }
//        else
//        {
//            // Not the last step of a fade with fade time. We should just update
//            // the fade counter by applying the increment/decrement
//            if (daliStateFlags.fadeUp == 1)
//            {
//                // We should be fading up, thus we add fadeDelta
//                fadeAccumulator.counter += fadeDelta;
//                if (fadeAccumulator.byte[2] != 0)
//                {
//                    // Overflow. We faded up too much. See the description of
//                    // fade_counter for details
//                    fadeAccumulator.counter = 0x0000ffff;
//                }
//                if (fadeAccumulator.byte[1] > maxLevel)
//                {
//                    fadeAccumulator.byte[1] = maxLevel;
//                }
//            }
//            else
//            {
//                fadeAccumulator.counter -= fadeDelta;
//                if (fadeAccumulator.byte[2] != 0)
//                {
//                    // Underflow. We faded down too much. See the description of
//                    // fade_counter for details
//                    fadeAccumulator.counter = 0x00000000;
//                }
//                if (fadeAccumulator.byte[1] < minLevel)
//                {
//                    fadeAccumulator.byte[1] = minLevel;
//                }
//            }
//            // Set the lamp power to the relevant part of the accumulator. See
//            // the description of fade_counter for detailss
//            actualArcPower = fadeAccumulator.byte[1];
//        }
//    }
//    else
//    {
//        // No fade running
//        status.fadeRunning = 0;
//    }
//}


static void idali_processCommand(uint8_t address, uint8_t command)
{
    // The command may be addressed to this Control Gear, to every Control
    // Gear or to some other Control Gear. This bunch of flags will keep this
    // sort of information.
    tdali_flags_command f;

    // Some commands are of the form
    // [1 start bit][8 bit address][4 bit command][4 bit parameter]
    // For these, this variable will hold the parameter
    uint8_t parameter;

    // The NVMemoryDALIBankMemoryRead() and NVMemoryDALIBankMemoryWrite() return
    // values end up in this variable for status checks
    uint16_t data;

    // Clear all addressing flags
    f.All = 0;

    // Firstly check if this device is addressed, since we need to know this
    // in case we were expecting the 2nd command of a "send twice" group.
    if ((address > 0xA0) && ((address & 0xFE) != 0xFE))
    {
        // This is one of the commands that does not include an address field
        // and should be interpreted by all Control Gears, irrespective of their
        // addresses.
        f.publicAddress = 1;
        f.deviceAddressed = 1;
    }
    else if (((address & 0xFE) == 0xFE) ||                  // Broadcast command
             ((address & 0xFE) == (shortAddress & 0xFE)) || // Short address match
             (idali_doesGroupMatch(address) != 0))          // Group address match
    {
        // This is either a broadcast command, a command sent to our short
        // address or to a group that we belong to. Either way, we are being
        // targeted with it.
        f.individualAddress = 1;
        f.deviceAddressed = 1;
    }

#ifdef DALI_USE_DEVICE_TYPE_6
    // During Reference system power all other commands except query commands
    // and TERMINATE should be ignored. Although not mentioned in the
    // standard, the "ENABLE DEVICE TYPE 6" command also needs to be allowed
    // since it gives access to LED specific queries.
    if ((f.deviceAddressed == 1) && (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING))
    {
        if (!(((address == 0xA1) && (command == 0x00)) ||                               // TERMINATE should still be allowed
              ((address == 0xC1) && (command == 0x06)) ||                               // ENABLE DEVICE TYPE 6 should still be allowed
              ((f.individualAddress == 1) && (command >= 0xED)) ||                      // General query commands
              ((f.individualAddress == 1) && (command >= 0x90) && (command < 0xB0))))   // DEV TYPE 6 query commands
        {
            // If the command we received is NOT one of the allowed ones, abort.
            return;
        }
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

    // Create a latch for the write enabled state. Since the device is addressed
    // most commands would now clear the write enabled state. However, the
    // "WRITE MEMORY LOCATION" command needs to know the status of the write
    // enabled bit. Using this mechanism we can clear the write enabled state
    // for the following commands (and if the current command is one that
    // reenables it, it can do so) but when decoding the command we can use the
    // old value of the write enabled state (as set by the previous command).
    daliStateFlags.writeEnabledOld = daliStateFlags.writeEnabled;
    daliStateFlags.writeEnabled = 0;

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

    if ((lastCommand == command) && (lastAddress == address) && (commandTimeout_ms != 0))
    {
        // Signal that this is a repeated command and also clear timer such that
        // we can't send a command 3 times and have it executed twice.
        f.commandRepeated = 1;
        commandTimeout_ms = 0;
    }

#ifdef DALI_USE_DEVICE_TYPE_6
    if (f.deviceAddressed == 1)
    {
        // Create a latch for the enable device type bit, working similarly to
        // the one for memory write enable.
        daliStateFlags.enabledDeviceTypeOld = daliStateFlags.enabledDeviceType;
        daliStateFlags.enabledDeviceType = 0;
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */

    if ((f.commandRepeated == 1) || (commandTimeout_ms == 0))
    {
        // We end up here if we should pay attention to this command, i.e. if
        // either this is the 2nd one of a "send twice" sequence that has been
        // addressed to us, or if no command that started the 100ms timer was
        // sent previously.

        // Check if this is one of those unaddressed commands that should be
        // listened to by everyone.
        // The full 16bit command is address:command, but in this case the type
        // of command to be executed is encoded in the address field and any
        // parameter is in the command field.
        if (f.publicAddress == 1)
        {
            switch (address)
            {
                case 0xA1:      // TERMINATE
                    if (command == 0x00)
                    {
                        // Cancel the initialise 15 minute timer
                        initialiseTimeout_100ms = 0;
#ifdef DALI_USE_DEVICE_TYPE_6
                        // And stop the reference system power measurement if
                        // it's running.
                        if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING)
                        {
                            idali_stopReferenceSystemPower();
                        }
#endif /* DALI_USE_DEVICE_TYPE_6 */
                    }
                    break;
                case 0xA3:      // DATA TRANSFER REGISTER (DTR)
                    // Load value into the DTR
                    DTR = command;
                    break;
                case 0xA5:      // INITIALISE
                    if (f.commandRepeated == 1)
                    {
                        // Command received twice within 100ms, so we need to
                        // start the 15 minute timer and set some default
                        // values.
                        initialiseTimeout_100ms = 15 * 60 * 10;
                        daliStateFlags.withdrawFromCompare = 0;
                        daliStateFlags.physicalSelectionEnabled = 0;
                        // We also need to start an identification procedure
                        // that can be defined by the manufacturer.
                        idali_startIdentificationProcedure();
                    }
                    else if ((command == 0) ||
                            ((command == 0xFF) && (shortAddress == 0xFF)) ||
                            (command == shortAddress))
                    {
                        // Command has not been received twice within 100ms, so
                        // we start the timer and wait for the next one.
                        commandTimeout_ms = 100;
                    }
                    break;
                case 0xA7:      // RANDOMISE
                    if (f.commandRepeated == 1)
                    {
                        // Command received twice within 100ms, so we need to
                        // pull random data from the application layer and store
                        // it as our random address. Also store it to
                        // non-volatile memory.
                        randomAddressH = random_byte();
                        if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H) != randomAddressH)
                        {
                            nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_H, randomAddressH);
                        }
                        randomAddressM = random_byte();
                        if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M) != randomAddressM)
                        {
                            nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_M, randomAddressM);
                        }
                        randomAddressL = random_byte();
                        if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L) != randomAddressL)
                        {
                            nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_L, randomAddressL);
                        }
                        daliStateFlags.withdrawFromCompare = 0;
                    }
                    else if (command == 0x00)
                    {
                        // This command is only valid in the form 0xA700. It may
                        // seem strange that the check for the lower 8 bits is
                        // only done here and not when the command is repeated,
                        // but whenever f.commandRepeated == 1 we know for sure
                        // that the command received is identical to the
                        // previous one (where this check has been made).
                        if (initialiseTimeout_100ms != 0)
                        {
                            // Command has not been received twice within 100ms,
                            // so we start the timer and wait for the next one.
                            commandTimeout_ms = 100;
                        }
                    }
                    break;
                case 0xA9:      // COMPARE
                    // See IEC 62386-102, 11.4.3 (page 36).
                    if (command == 0x00)
                    {
                        // Command is actually 0xA900, thus we should execute it
                        if (initialiseTimeout_100ms != 0)
                        {
                            if ((daliStateFlags.withdrawFromCompare == 0) && (daliStateFlags.physicalSelectionEnabled == 0))
                            {
                                // If compare is enabled, check our random
                                // address against the search address
                                if ((randomAddressH < searchAddressH) ||
                                    ((randomAddressH == searchAddressH) && (randomAddressM < searchAddressM)) ||
                                    ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL <= searchAddressL)))
                                {
                                    // Random address is less than or equal to
                                    // the search address.
                                    idali_sendBackwardFrame(0xFF);
                                }
                            }
                        }
                    }
                    break;
                case 0xAB:      // WITHDRAW
                    if (command == 0x00)
                    {
                        if (initialiseTimeout_100ms != 0)
                        {
                            if ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL))
                            {
                                // We are being excluded from the compare
                                // process, but NOT from the initialisation
                                // process
                                daliStateFlags.withdrawFromCompare = 1;
                            }
                        }
                    }
                    break;
                case 0xB1:      // SEARCHADDRH
                    if (initialiseTimeout_100ms != 0)
                    {
                        searchAddressH = command;
                    }
                    break;
                case 0xB3:      // SEARCHADDRM
                    if (initialiseTimeout_100ms != 0)
                    {
                        searchAddressM = command;
                    }
                    break;
                case 0xB5:      // SEARCHADDRL
                    if (initialiseTimeout_100ms != 0)
                    {
                        searchAddressL = command;
                    }
                    break;
                case 0xB7:      // PROGRAM SHORT ADDRESS
                    if (initialiseTimeout_100ms != 0)
                    {
                        // We should store this short address if physical
                        // selection has been enabled and we are being
                        // physically selected or if the random address
                        // equals the search address
                        if (daliStateFlags.physicalSelectionEnabled == 1)
                        {
                            if ((idali_getLampStatus() & LAMP_STATUS_OPEN_CIRCUIT) != 0)
                            {
                                idali_setShortAddress(command);
                            }
                        }
                        else if ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL))
                        {
                            idali_setShortAddress(command);
                        }
                    }
                    break;
                case 0xB9:      // VERIFY SHORT ADDRESS
                    if (initialiseTimeout_100ms != 0)
                    {
                        if (command == shortAddress)
                        {
                            idali_sendBackwardFrame(0xFF);
                        }
                    }
                    break;
                case 0xBB:      // QUERY SHORT ADDRESS
                    if (initialiseTimeout_100ms != 0)
                    {
                        if (daliStateFlags.physicalSelectionEnabled == 1)
                        {
                            if ((idali_getLampStatus() & LAMP_STATUS_OPEN_CIRCUIT) != 0)
                            {
                                idali_sendBackwardFrame(shortAddress);
                            }
                        }
                        else if ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL))
                        {
                            idali_sendBackwardFrame(shortAddress);
                        }
                    }
                    break;
                case 0xBD:      // PHYSICAL SELECTION
                    // See IEC 62386-102, 11.4.3 (page 37).
                    if (initialiseTimeout_100ms != 0)
                    {
                        daliStateFlags.physicalSelectionEnabled = 1;
                    }
                    break;
#ifdef DALI_USE_DEVICE_TYPE_6
                case 0xC1:      // ENABLE DEVICE TYPE X
                    if (command == 6)
                    {
                        daliStateFlags.enabledDeviceType = 1;
                    }
                    break;
#endif /* DALI_USE_DEVICE_TYPE_6 */

                case 0xC3:      // DATA TRANSFER REGISTER 1 (DTR1)
                    DTR1 = command;
                    break;
                case 0xC5:      // DATA TRANSFER REGISTER 2 (DTR2)
                    DTR2 = command;
                    break;
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                case 0xC7:      // WRITE MEMORY LOCATION
                    // See IEC 62386-102, 11.4.4 (page 38).
                    // We now check the previous state of the write enabled
                    // flag, since at the beginning of this function we've reset
                    // the "new" value to 0.
//                    printf("wd\n");
                    if (daliStateFlags.writeEnabledOld == 1)
                    {
//                        printf("f\n");
                        // Keep write enabled state. Set the "new" value to 1
                        daliStateFlags.writeEnabled = 1;
                        
                        // Writing to bank 0 is not allowed
                        if (DTR1 > 0)
                        {
                            // Determine if the (supposed) memory location is
                            // locked.
                            if (((DTR1 == 1) && (DTR == 0x0F)) ||   // This location is not lockable
                                (DTR == 0x02) ||                    // This location is the lock byte
                                ((DTR >= 2) && (nvmem_daliBankMemoryRead(DTR1, 0x02) == 0x0055)))  // This memory is unlocked
                            {
                                // Try to write memory. If the address is out of
                                // range we'll know that from the return value.
                                data = nvmem_daliBankMemoryWrite(DTR1, DTR, command);
                                DTR++;
                                if (data != BANK_MEMORY_WRITE_CANNOT_WRITE)
                                {
                                    // Write succeeded. Backchannel reply
                                    idali_sendBackwardFrame(command);

                                    // Flag this memory bank as changed, such
                                    // that the DALI machine will know to
                                    // recompute its checksums
                                    checksumChange |= 1 << DTR1;
                                    if (data == BANK_MEMORY_WRITE_OK_LAST_LOC)
                                    {
                                        // This is the last accessible location
                                        // in the bank. We should thus disable
                                        // the write state.
                                        daliStateFlags.writeEnabled = 0;
                                    }
                                }
                            }
                        }
                    }
                    break;
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
            }
        }
        else if (f.individualAddress == 1)
        {
            // These commands are sent by address. This could be a broadcast
            // address, an individual address or a group which we belong to.
            if ((address & 0x01) == 0x00)
            {
                // DIRECT ARC POWER CONTROL
                // See IEC 62386-102, 11.1.1 (page 27).
                idali_setArcPower(command, FADE_FADE_TIME);
            }
            else
            {
                // This first switch handles commands that span ranges, such as
                // YAAA AAA1 0001 XXXX -- GO TO SCENE XXXX. If the command in
                // question is not one of these we'll fall through the "default"
                // case of the switch statement and there we have a new switch
                // with the individual commands that are left.

                // Just in case the command includes a parameter, obtain it now
                // such that it won't need to be recomputed for every case
                parameter = command & 0x0F;

                switch (command & 0xF0)
                {
                    case 0x10:  // GO TO SCENE
                        // Check if we belong to the scene. This check is being
                        // made since idali_setArcPower(255, FADE_FADE_TIME)
                        // would mean "stop fading" and this may not be what we
                        // are trying to do.
                        if (scenes[parameter] != 255)
                        {
                            idali_setArcPower(scenes[parameter], FADE_FADE_TIME);
                        }
                        break;
                    case 0x40:  // STORE THE DTR AS SCENE
                        if (f.commandRepeated == 1)
                        {
                            if (DTR != scenes[parameter])
                            {
                                scenes[parameter] = DTR;
                                if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + parameter) != DTR)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + parameter, DTR);
                                }
                            }
                        }
                        else
                        {
                            commandTimeout_ms = 100;
                        }
                        break;
                    case 0x50:  // REMOVE FROM SCENE
                        if (f.commandRepeated == 1)
                        {
                            if (scenes[parameter] != 255)
                            {
                                scenes[parameter] = 255;
                                if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + parameter) != 255)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + parameter, 255);
                                }
                            }
                        }
                        else
                        {
                            commandTimeout_ms = 100;
                        }
                        break;
                    case 0x60:  // ADD TO GROUP
                        if (f.commandRepeated == 1)
                        {
                            // Set the bit corresponding the the "parameter"
                            // group in groupHigh:gropuLow
                            if (parameter < 8)
                            {
                                groupLow |= groupMask[parameter];
                                if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != groupLow)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, groupLow);
                                }
                            }
                            else
                            {
                                groupHigh |= groupMask[parameter - 8];
                                if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != groupHigh)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, groupHigh);
                                }
                            }
                        }
                        else
                        {
                            commandTimeout_ms = 100;
                        }
                        break;
                    case 0x70:  // REMOVE FROM GROUP
                        if (f.commandRepeated == 1)
                        {
                            // Clear the bit corresponding the the "parameter"
                            // group in groupHigh:gropuLow
                            if (parameter < 8)
                            {
                                groupLow &= ~groupMask[parameter];
                                if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != groupLow)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, groupLow);
                                }
                            }
                            else
                            {
                                groupHigh &= ~groupMask[parameter - 8];
                                if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != groupHigh)
                                {
                                    nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, groupHigh);
                                }
                            }
                        }
                        else
                        {
                            commandTimeout_ms = 100;
                        }
                        break;
                    case 0xB0:  // QUERY SCENE LEVEL (SCENES 0 - 15)
                        // Reply with the value stored for scene number
                        // "parameter"
                        idali_sendBackwardFrame(scenes[parameter]);
                        break;
                    default:
                        // This is not a command that spans ranges. We now need
                        // to check every command and act accordingly.
                        switch (command)
                        {
                            case 0x00:  // OFF
                                // See IEC 62386-102, 11.1.2, Command 9
                                // (page 29).
                                dapcTimeout_ms = 0;
                                idali_setArcPower(0, FADE_NONE);
                                break;
                            case 0x01:  // UP
                                dapcTimeout_ms = 0;
                                if  (actualArcPower > 0)
                                {
                                    // Do not allow "UP" to turn lamp on
                                    idali_setArcPower(1, FADE_FADE_RATE);
                                }
                                break;
                            case 0x02:  // DOWN
                                dapcTimeout_ms = 0;
                                if  (actualArcPower > minLevel)
                                {
                                    // Do not allow "DOWN" to turn lamp off
                                    idali_setArcPower(0, FADE_FADE_RATE);
                                }
                                break;
                            case 0x03:  // STEP UP
                                dapcTimeout_ms = 0;
                                if (actualArcPower != 0)
                                {
                                    // Do not allow "STEP UP" to turn lamp on
                                    idali_setArcPower(actualArcPower + 1, FADE_NONE);
                                }
                                break;
                            case 0x04:  // STEP DOWN
                                dapcTimeout_ms = 0;
                                if (actualArcPower > 1)
                                {
                                    // Do not allow "STEP DOWN" to turn lamp off
                                    idali_setArcPower(actualArcPower - 1, FADE_NONE);
                                }
                                break;
                            case 0x05:  // RECALL MAX LEVEL
                                dapcTimeout_ms = 0;
                                idali_setArcPower(maxLevel, FADE_NONE);
                                break;
                            case 0x06:  // RECALL MIN LEVEL
                                dapcTimeout_ms = 0;
                                idali_setArcPower(minLevel, FADE_NONE);
                                break;
                            case 0x07:  // STEP DOWN AND OFF
                                dapcTimeout_ms = 0;
                                if (actualArcPower == minLevel)
                                {
                                    idali_setArcPower(0, FADE_NONE);
                                }
                                else if (actualArcPower > minLevel)
                                {
                                    idali_setArcPower(actualArcPower - 1, FADE_NONE);
                                }
                                break;
                            case 0x08:  // ON AND STEP UP
                                dapcTimeout_ms = 0;
                                if (actualArcPower == 0)
                                {
                                    idali_setArcPower(minLevel, FADE_NONE);
                                }
                                else
                                {
                                    idali_setArcPower(actualArcPower + 1, FADE_NONE);
                                }
                                break;
                            case 0x09:  // ENABLE DAPC SEQUENCE
                                dapcTimeout_ms = 200;
                                break;
                            case 0x20:  // RESET
                                if (f.commandRepeated == 1)
                                {
                                    // Perform a DALI reset
                                    idali_reset();
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x21:  // STORE ACTUAL LEVEL IN THE DTR
                                if (f.commandRepeated == 1)
                                {
                                    DTR = actualArcPower;
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2A:  // STORE THE DTR AS MAX LEVEL
                                if (f.commandRepeated == 1)
                                {
                                    // Make sure the level we set is a legal one
                                    if (DTR < minLevel)
                                    {
                                        maxLevel = minLevel;
                                    }
                                    else if (DTR == 255)
                                    {
                                        maxLevel = 254;
                                    }
                                    else
                                    {
                                        maxLevel = DTR;
                                    }

                                    // If it changed, update the non-volatile
                                    // memory as well
                                    if (nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL) != maxLevel)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_MAXIMUM_LEVEL, maxLevel);
                                    }

                                    // See IEC 62386-102, 11.2.2, (page 31)
                                    if (actualArcPower > maxLevel)
                                    {
                                        idali_setArcPower(maxLevel, FADE_NONE);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2B:  // STORE THE DTR AS MIN LEVEL
                                if (f.commandRepeated == 1)
                                {
                                    // Make sure the level we set is a legal one
                                    if (DTR < physicalMinimumLevel)
                                    {
                                        minLevel = physicalMinimumLevel;
                                    }
                                    else if ((DTR > maxLevel) || (DTR == 255))
                                    {
                                        minLevel = maxLevel;
                                    }
                                    else
                                    {
                                        minLevel = DTR;
                                    }

                                    // If it changed, update the non-volatile
                                    // memory as well
                                    if (nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL) != minLevel)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_MINIMUM_LEVEL, minLevel);
                                    }

                                    // See IEC 62386-102, 11.2.2, (page 31)
                                    if ((actualArcPower < minLevel) && (actualArcPower != 0))
                                    {
                                        idali_setArcPower(minLevel, FADE_NONE);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2C:  // STORE THE DTR AS SYSTEM FAILURE LEVEL
                                if (f.commandRepeated == 1)
                                {
                                    if (systemFailureLevel != DTR)
                                    {
                                        systemFailureLevel = DTR;
                                        nvmem_daliWriteByte(NVMEMORY_SYSTEM_FAILURE_LEVEL, DTR);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2D:  // STORE THE DTR AS POWER ON LEVEL
                                if (f.commandRepeated == 1)
                                {
                                    if (powerOnLevel != DTR)
                                    {
                                        powerOnLevel = DTR;
                                        nvmem_daliWriteByte(NVMEMORY_POWER_ON_LEVEL, DTR);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2E:  // STORE THE DTR AS FADE TIME
                                if (f.commandRepeated == 1)
                                {
                                    // Make sure we store a valid value
                                    if (DTR < 16)
                                    {
                                        fadeTime = DTR;
                                    }
                                    else
                                    {
                                        fadeTime = 15;
                                    }

                                    if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != fadeTime)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_FADE_TIME, fadeTime);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x2F:  // STORE THE DTR AS FADE RATE
                                if (f.commandRepeated == 1)
                                {
                                    // Make sure we store a valid value
                                    if (DTR == 0)
                                    {
                                        fadeRate = 1;
                                    }
                                    else if (DTR > 15)
                                    {
                                        fadeRate = 15;
                                    }
                                    else
                                    {
                                        fadeRate = DTR;
                                    }

                                    if (nvmem_daliReadByte(NVMEMORY_FADE_RATE) != fadeRate)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_FADE_RATE, fadeRate);
                                    }
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0x80:  // STORE DTR AS SHORT ADDRESS
                                if (f.commandRepeated == 1)
                                {
                                    // Let the function check the validity of
                                    // the value stored in DTR
                                    idali_setShortAddress(DTR);
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                            case 0x81:  // ENABLE WRITE MEMORY
                                if (f.commandRepeated == 1)
                                {
//                                    printf("we\n");
                                    // The next command will have the memory
                                    // writing enabled
                                    daliStateFlags.writeEnabled = 1;
                                }
                                else
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                            case 0x90:  // QUERY STATUS
                                idali_sendBackwardFrame(status.All);
                                break;
                            case 0x91:  // QUERY CONTROL GEAR
                                idali_sendBackwardFrame(0xFF);
                                break;
                            case 0x92:  // QUERY LAMP FAILURE
                                if (status.lampFailure != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x93:  // QUERY LAMP POWER ON
                                if (status.lampArcPowerOn != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x94:  // QUERY LIMIT ERROR
                                if (status.limitError != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x95:  // QUERY RESET STATE
                                if (status.resetState != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x96:  // QUERY MISSING SHORT ADDRESS
                                if (status.missingShortAddress != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x97:  // QUERY VERSION NUMBER
                                idali_sendBackwardFrame(1);
                                break;
                            case 0x98:  // QUERY CONTENT DTR
                                idali_sendBackwardFrame(DTR);
                                break;
                            case 0x99:  // QUERY DEVICE TYPE
                                idali_sendBackwardFrame(DEVICE_TYPE);
                                break;
                            case 0x9A:  // QUERY PHYSICAL MINIMUM LEVEL
                                idali_sendBackwardFrame(physicalMinimumLevel);
                                break;
                            case 0x9B:  // QUERY POWER FAILURE
                                if (status.powerFailure != 0)
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                break;
                            case 0x9C:  // QUERY CONTENT DTR1
                                idali_sendBackwardFrame(DTR1);
                                break;
                            case 0x9D:  // QUERY CONTENT DTR2
                                idali_sendBackwardFrame(DTR2);
                                break;
                            case 0xA0:  // QUERY ACTUAL LEVEL
#ifdef DALI_USE_DEVICE_TYPE_6
                                if ((status.lampFailure == 1) || (failureStatus.thermalOverload == 1) || (failureStatus.thermalShutDown == 1))
#else
                                if (status.lampFailure == 1)
#endif /* DALI_USE_DEVICE_TYPE_6 */
                                {
                                    idali_sendBackwardFrame(0xFF);
                                }
                                else
                                {
                                    idali_sendBackwardFrame(actualArcPower);
                                }
                                break;
                            case 0xA1:  // QUERY MAX LEVEL
                                idali_sendBackwardFrame(maxLevel);
                                break;
                            case 0xA2:  // QUERY MIN LEVEL
                                idali_sendBackwardFrame(minLevel);
                                break;
                            case 0xA3:  // QUERY POWER ON LEVEL
                                idali_sendBackwardFrame(powerOnLevel);
                                break;
                            case 0xA4:  // QUERY SYSTEM FAILURE LEVEL
                                idali_sendBackwardFrame(systemFailureLevel);
                                break;
                            case 0xA5:  // QUERY FADE TIME/FADE RATE
                                idali_sendBackwardFrame((fadeTime << 4) | fadeRate);
                                break;
                            case 0xC0:  // QUERY GROUPS 0-7
                                idali_sendBackwardFrame(groupLow);
                                break;
                            case 0xC1:  // QUERY GROUPS 8-15
                                idali_sendBackwardFrame(groupHigh);
                                break;
                            case 0xC2:  // QUERY RANDOM ADDRESS (H)
                                idali_sendBackwardFrame(randomAddressH);
                                break;
                            case 0xC3:  // QUERY RANDOM ADDRESS (M)
                                idali_sendBackwardFrame(randomAddressM);
                                break;
                            case 0xC4:  // QUERY RANDOM ADDRESS (L)
                                idali_sendBackwardFrame(randomAddressL);
                                break;
                            case 0xC5:  // READ MEMORY LOCATION
                                // DTR1 == bank
                                // DTR == location
                                data = nvmem_daliBankMemoryRead(DTR1, DTR);
                                if ((data & 0xFF00) == 0)
                                {
                                    // Read OK, no error bits set
                                    idali_sendBackwardFrame(data & 0xFF);
                                    DTR++;
                                    data = nvmem_daliBankMemoryRead(DTR1, DTR);
                                    if ((data & 0xFF00) == 0)
                                    {
                                        // Next byte read OK, no error bits set
                                        DTR2 = (uint8_t)data;
                                    }
                                }
                                break;
#ifdef DALI_USE_DEVICE_TYPE_6
                            case 0xE0:  // REFERENCE SYSTEM POWER
                                if (f.commandRepeated == 1)
                                {
                                    // (Mis)use the 8bit variable "parameter" to
                                    // store the bits from the non-volatile
                                    // memory temporarily in order to flip some
                                    // bits (if necessary)
                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
                                    if (failureStatus.currentProtectorActive == 1)
                                    {
                                        failureStatus.referenceMeasurementFailed = 1;
                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_REF_FAIL) == 0)
                                        {
                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_REF_FAIL);
                                        }
                                    }
                                    else
                                    {
                                        failureStatus.referenceMeasurementFailed = 0;
                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_REF_FAIL) != 0)
                                        {
                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_REF_FAIL));
                                        }
                                        idali_startReferenceSystemPower();
                                    }
                                }
                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0xE1:  // ENABLE CURRENT PROTECTOR
                                if (f.commandRepeated == 1)
                                {
                                    daliLedStateFlags.currentProtectorEnabled = 1;
                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
                                    if ((parameter & NVMEMORY_MULTIPLE_BITS_CP_EN) == 0)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_CP_EN);
                                    }
                                }
                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0xE2:  // DISABLE CURRENT PROTECTOR
                                if (f.commandRepeated == 1)
                                {
                                    daliLedStateFlags.currentProtectorEnabled = 0;
                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
                                    if ((parameter & NVMEMORY_MULTIPLE_BITS_CP_EN) != 0)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_CP_EN));
                                    }
                                }
                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0xE3:  // SELECT DIMMING CURVE
                                if (f.commandRepeated == 1)
                                {
                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
                                    if (DTR == 0)
                                    {
                                        operatingMode.nonLogDimmingCurve = 0;
                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_LIN_DIM) != 0)
                                        {
                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_LIN_DIM));
                                        }
                                        physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
                                    }
                                    else if (DTR == 1)
                                    {
                                        operatingMode.nonLogDimmingCurve = 1;
                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_LIN_DIM) == 0)
                                        {
                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_LIN_DIM);
                                        }
                                        physicalMinimumLevel = idali_getLampNonLogPhysicalMinimum();
                                    }
                                }
                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0xE4:  // STORE DTR AS FAST FADE TIME
                                if (f.commandRepeated == 1)
                                {
                                    // Make sure we are using a valid value
                                    if ((DTR == 0) || ((DTR >= MIN_FAST_FADE_TIME) && (DTR <= 27)))
                                    {
                                        fastFadeTime = DTR;
                                    }
                                    else if ((DTR > 0) && (DTR < MIN_FAST_FADE_TIME))
                                    {
                                        fastFadeTime = MIN_FAST_FADE_TIME;
                                    }
                                    else if (DTR > 27)
                                    {
                                        fastFadeTime = 27;
                                    }

                                    if (nvmem_daliReadByte(NVMEMORY_FAST_FADE_TIME) != fastFadeTime)
                                    {
                                        nvmem_daliWriteByte(NVMEMORY_FAST_FADE_TIME, fastFadeTime);
                                    }
                                }
                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    commandTimeout_ms = 100;
                                }
                                break;
                            case 0xED:  // QUERY GEAR TYPE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(GEAR_TYPE);
                                }
                                break;
                            case 0xEE:  // QUERY DIMMING CURVE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (operatingMode.nonLogDimmingCurve == 1)
                                    {
                                        idali_sendBackwardFrame(1);
                                    }
                                    else
                                    {
                                        idali_sendBackwardFrame(0);
                                    }
                                }
                                break;
                            case 0xEF:  // QUERY POSSIBLE OPERATING MODES
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(POSSIBLE_OPERATING_MODES);
                                }
                                break;
                            case 0xF0:  // QUERY FEATURES
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(FEATURES);
                                }
                                break;
                            case 0xF1:  // QUERY FAILURE STATUS
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(failureStatus.All);
                                }
                                break;
                            case 0xF2:  // QUERY SHORT CIRCUIT
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.shortCircuit == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF3:  // QUERY OPEN CIRCUIT
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.openCircuit == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF4:  // QUERY LOAD DECREASE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.loadDecrease == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF5:  // QUERY LOAD INCREASE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.loadIncrease == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF6:  // QUERY CURRENT PROTECTOR ACTIVE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.currentProtectorActive == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF7:  // QUERY THERMAL SHUTDOWN
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.thermalShutDown == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF8:  // QUERY THERMAL OVERLOAD
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.thermalOverload == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xF9:  // QUERY REFERENCE RUNNING
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xFA:  // QUERY REFERENCE MEASUREMENT FAILED
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (failureStatus.referenceMeasurementFailed == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xFB:  // QUERY CURRENT PROTECTOR ENABLED
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    if (daliLedStateFlags.currentProtectorEnabled == 1)
                                    {
                                        idali_sendBackwardFrame(0xFF);
                                    }
                                }
                                break;
                            case 0xFC:  // QUERY OPERATING MODE
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(operatingMode.All);
                                }
                                break;
                            case 0xFD:  // QUERY FAST FADE TIME
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(fastFadeTime);
                                }
                                break;
                            case 0xFE:  // QUERY MIN FAST FADE TIME
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(MIN_FAST_FADE_TIME);
                                }
                                break;
                            case 0xFF:  // QUERY EXTENDED VERSION NUMBER
                                if (daliStateFlags.enabledDeviceTypeOld == 1)
                                {
                                    idali_sendBackwardFrame(1);
                                }
                                break;
#endif /* DALI_USE_DEVICE_TYPE_6 */
                        }
                        break;
                }
            }
        }
    }
    else if (f.deviceAddressed == 1)
    {
        // This branch gets executed if this command is addressed to us and came
        // < 100ms after the first of a pair of commands that should be sent
        // twice within 100ms.
        // This means we should ignore this command and cancel the timer that
        // was started. See IEC 62386-102, 11.2.1 (page 30).
        commandTimeout_ms = 0;
    }

    if (f.deviceAddressed == 1)
    {
        // Store the last command in order to be able to check for commands
        // being sent twice.
        lastCommand = command;
        lastAddress = address;
    }
}

/**********************Function implementations*(END)**************************/

