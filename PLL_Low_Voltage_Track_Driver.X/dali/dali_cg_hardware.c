/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_hardware.c
 *
 * About:
 *  Hardware definition layer for the DALI example.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Data                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/


#include "dali_cg_hardware.h"
//#include "../mcc_generated_files/tmr2.h"
#include "../mcc_generated_files/ext_int.h"
#include <stdbool.h>



#define DALI_RX                         PORTBbits.RB15      // RX line input register bit
#define DALI_RX_TRIS                    TRISBbits.TRISB15      // RX line tristate register bit
#define DALI_TX                         LATCbits.LATC9       // TX line output register bit
#define DALI_TX_TRIS                    TRISCbits.TRISC9      // TX line tristate register bit


// There's a layer of inversion in the optocoupelrs' transistors on the board
// and we define these two symbols such that other hardware configurations can
// easily be adopted.
#define DALI_RX_HI                         1
#define DALI_RX_LO                         0

#define DALI_TX_HI                         0
#define DALI_TX_LO                         1

/**********************Function implementations********************************/


inline void dalihw_teTimerInit()
{
 // done in tmr2.c

}


inline void dalihw_teTimerOn()
{
    TMR2_Start();
}


inline void dalihw_teTimerOff()
{
    TMR2_Stop();
}


inline void dalihw_teTimerClearInterrupt()
{
   // done in tmr2.c
}


inline uint16_t dalihw_teTimerGetVal()
{
   return TMR2_Counter16BitGet();
}


inline void dalihw_teTimerSetVal(uint16_t val)
{
    TMR2_Counter16BitSet (val);
}



inline void dalihw_extInterruptSetFallingDALI()
{
//    INTCONbits.INT0EP = DALI_LO;
    EX_INT0_InterruptDisable();
    EX_INT0_InterruptFlagClear();  
    EX_INT0_NegativeEdgeSet();
    EX_INT0_InterruptEnable();
}


inline void dalihw_extInterruptSetRisingDALI()
{
//    INTCONbits.INT0EP = DALI_HI;
    EX_INT0_InterruptDisable();
    EX_INT0_InterruptFlagClear();   
    EX_INT0_PositiveEdgeSet();
    EX_INT0_InterruptEnable();
}



inline void dalihw_extInterruptToggleEdge()
{
    EX_INT0_InterruptDisable();
    EX_INT0_InterruptFlagClear();   
    EX_INT0_EdgeInvert();
    EX_INT0_InterruptEnable();
}


inline uint8_t dalihw_isDALILineLow()
{
    return (DALI_RX == DALI_RX_LO);
}


inline void dalihw_setDALILineLow()
{
    DALI_TX = DALI_TX_LO;

}


inline void dalihw_setDALILineHigh()
{
    DALI_TX = DALI_TX_HI;
}


inline void dalihw_init()
{
    dalihw_setDALILineHigh();
    dalihw_extInterruptSetFallingDALI();
}


/**********************Function implementations*(END)**************************/
