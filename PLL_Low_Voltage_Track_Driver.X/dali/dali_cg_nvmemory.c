/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_nvmemory.c
 *
 * About:
 *  Non-volatile memory implementation for the DALI example. This uses on-chip
 *  EEPROM to store data.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/


#include "dali_cg_nvmemory.h"
#include "dali_cg_config.h"

#include "./dali_core/dali_cg_machine.h"
#include "../eeprom.h"

#ifdef DALI_USE_DEVICE_TYPE_6
//#include "lamp_reference_system_power.h"
#endif /* DALI_USE_DEVICE_TYPE_6 */


// Note that the memory banks as defined by the DALI standard are not related to
// the banks defined by the PIC architecture. For further description of the
// DALI memory banks please refer to IEC62386-102, section 9.8.
// The number of banks in this implementation of the library is currently
// limited to 8 (checksumChange flags size).


// DALI bank 0 values. This bank's structure is defined by the standard up to
// location 0x0E. These first 15 values are read-only, further locations and
// their writability can be freely chosen by the control gear manufacturer.
// However, the present DALI machine does not allow any writing to bank 0. As
// such, in this application example we only implement these read-only locations
// for bank 0 such that we can store these values in program space, saving
// the EEPROM for actual writable data. Note that other implementations might
// choose to use EEPROM for these read-only values as well to save program
// space.
// Bank 0 is the only mandatory memory bank and it is read-only, thus the
// application does not need to be able to store user data to non-volatile
// memory (but it does need to store the DALI variables to non-volatile memory).
// Please refer to the DALI documentation for further information regarding the
// data banks.
#define MEM_BANK0_SIZE                              0x0F


#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// This demo application implements 2 more banks apart from the mandatory
// bank 0.
#define MEM_BANK0_LAST_ACCESSIBLE_BANK              2
#else
#define MEM_BANK0_LAST_ACCESSIBLE_BANK              0
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


// Locations in the non-volatile memory where each of these values should be
// stored.
#define MEM_BANK0_GTIN_BYTE_0                       1
#define MEM_BANK0_GTIN_BYTE_1                       2
#define MEM_BANK0_GTIN_BYTE_2                       3
#define MEM_BANK0_GTIN_BYTE_3                       4
#define MEM_BANK0_GTIN_BYTE_4                       5
#define MEM_BANK0_GTIN_BYTE_5                       6
#define MEM_BANK0_CG_FW_VERSION_MAJOR               1
#define MEM_BANK0_CG_FW_VERSION_MINOR               1
#define MEM_BANK0_SN_BYTE_1                         1
#define MEM_BANK0_SN_BYTE_2                         2
#define MEM_BANK0_SN_BYTE_3                         3
#define MEM_BANK0_SN_BYTE_4                         4


// The checksum for the memory bank is computed by the preprocessor from the
// values that will be written in the memory bank. If additional locations get
// implemented in bank 0, they need to be added to this checksum computation,
// otherwise DALI Control Devices accessing this bank might notice a checksum
// mismatch.
#define MEM_BANK0_CHECKSUM                  ((0 - \
                                                MEM_BANK0_LAST_ACCESSIBLE_BANK - \
                                                MEM_BANK0_GTIN_BYTE_0 - \
                                                MEM_BANK0_GTIN_BYTE_1 - \
                                                MEM_BANK0_GTIN_BYTE_2 - \
                                                MEM_BANK0_GTIN_BYTE_3 - \
                                                MEM_BANK0_GTIN_BYTE_4 - \
                                                MEM_BANK0_GTIN_BYTE_5 - \
                                                MEM_BANK0_CG_FW_VERSION_MAJOR - \
                                                MEM_BANK0_CG_FW_VERSION_MINOR - \
                                                MEM_BANK0_SN_BYTE_1 - \
                                                MEM_BANK0_SN_BYTE_2 - \
                                                MEM_BANK0_SN_BYTE_3 - \
                                                MEM_BANK0_SN_BYTE_4 \
                                                ) % 256)


/** \brief Actual definition of the bank 0 memory space.
 *
 * This array contains memory bank 0 and is implemented in program space to
 * save EEPROM.
 */
const uint8_t bank0mem[MEM_BANK0_SIZE] = {
    MEM_BANK0_SIZE - 1,
    MEM_BANK0_CHECKSUM,
    MEM_BANK0_LAST_ACCESSIBLE_BANK,
    MEM_BANK0_GTIN_BYTE_0,
    MEM_BANK0_GTIN_BYTE_1,
    MEM_BANK0_GTIN_BYTE_2,
    MEM_BANK0_GTIN_BYTE_3,
    MEM_BANK0_GTIN_BYTE_4,
    MEM_BANK0_GTIN_BYTE_5,
    MEM_BANK0_CG_FW_VERSION_MAJOR,
    MEM_BANK0_CG_FW_VERSION_MINOR,
    MEM_BANK0_SN_BYTE_1,
    MEM_BANK0_SN_BYTE_2,
    MEM_BANK0_SN_BYTE_3,
    MEM_BANK0_SN_BYTE_4};


#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// DALI bank 1 values. Bank 1 is optional. However, if it is implemented, it
// needs to have a fixed structure with at least 16 locations. Most of these
// locations are writable and some are lockable.
// While the size is not supposed to change, it is also written to EEPROM in
// this example for ease of access -- however, this is not a requirement.
#define MEM_BANK1_SIZE                              0x10
#define MEM_BANK1_LOCK_BYTE                         0
#define MEM_BANK1_OEM_GTIN_BYTE_0                   10
#define MEM_BANK1_OEM_GTIN_BYTE_1                   11
#define MEM_BANK1_OEM_GTIN_BYTE_2                   12
#define MEM_BANK1_OEM_GTIN_BYTE_3                   13
#define MEM_BANK1_OEM_GTIN_BYTE_4                   14
#define MEM_BANK1_OEM_GTIN_BYTE_5                   15
#define MEM_BANK1_OEM_SN_BYTE_1                     20
#define MEM_BANK1_OEM_SN_BYTE_2                     21
#define MEM_BANK1_OEM_SN_BYTE_3                     22
#define MEM_BANK1_OEM_SN_BYTE_4                     23
#define MEM_BANK1_SUBSYS_DEVNO                      50
#define MEM_BANK1_LAMP_TYPENO_L                     56
#define MEM_BANK1_LAMP_TYPENO                       62


// Automatic checksum computation by the preprocessor. This applies just to the
// origial values written to the memory, whenever something is changed the
// DALI state machine will update the checksum.
#define MEM_BANK1_CHECKSUM                  ((0 - \
                                                MEM_BANK1_LOCK_BYTE - \
                                                MEM_BANK1_OEM_GTIN_BYTE_0 - \
                                                MEM_BANK1_OEM_GTIN_BYTE_1 - \
                                                MEM_BANK1_OEM_GTIN_BYTE_2 - \
                                                MEM_BANK1_OEM_GTIN_BYTE_3 - \
                                                MEM_BANK1_OEM_GTIN_BYTE_4 - \
                                                MEM_BANK1_OEM_GTIN_BYTE_5 - \
                                                MEM_BANK1_OEM_SN_BYTE_1 - \
                                                MEM_BANK1_OEM_SN_BYTE_2 - \
                                                MEM_BANK1_OEM_SN_BYTE_3 - \
                                                MEM_BANK1_OEM_SN_BYTE_4 - \
                                                MEM_BANK1_SUBSYS_DEVNO - \
                                                MEM_BANK1_LAMP_TYPENO_L - \
                                                MEM_BANK1_LAMP_TYPENO \
                                                ) % 256)


/** \brief Definition of memory bank 1 in EEPROM data space.
 *
 * This array contains memory bank 1 and is implemented in EEPROM.
 */
uint8_t EEbank1mem[MEM_BANK1_SIZE] = {
    MEM_BANK1_SIZE - 1,
    MEM_BANK1_CHECKSUM,
    MEM_BANK1_LOCK_BYTE,
    MEM_BANK1_OEM_GTIN_BYTE_0,
    MEM_BANK1_OEM_GTIN_BYTE_1,
    MEM_BANK1_OEM_GTIN_BYTE_2,
    MEM_BANK1_OEM_GTIN_BYTE_3,
    MEM_BANK1_OEM_GTIN_BYTE_4,
    MEM_BANK1_OEM_GTIN_BYTE_5,
    MEM_BANK1_OEM_SN_BYTE_1,
    MEM_BANK1_OEM_SN_BYTE_2,
    MEM_BANK1_OEM_SN_BYTE_3,
    MEM_BANK1_OEM_SN_BYTE_4,
    MEM_BANK1_SUBSYS_DEVNO,
    MEM_BANK1_LAMP_TYPENO_L,
    MEM_BANK1_LAMP_TYPENO};





// DALI bank 2. The structure for banks >= 2 is not as strict as those for the
// first two.
#define MEM_BANK2_SIZE                              0x20
#define MEM_BANK2_LOCK_BYTE                         0

#define MEM_BANK2_CHECKSUM                  ((0 - \
                                                MEM_BANK2_LOCK_BYTE \
                                                ) % 256)


/** \brief Definition of memory bank 2 in EEPROM data space.
 *
 * This array contains memory bank 2 and is implemented in EEPROM.
 */
uint8_t EEbank2mem[MEM_BANK2_SIZE] = {
    MEM_BANK2_SIZE - 1,
    MEM_BANK2_CHECKSUM,
    MEM_BANK2_LOCK_BYTE
    };


#define MEM_BANK208_SIZE                              0x5
#define MEM_BANK208_LOCK_BYTE                         0
#define MEM_BANK208_CURRENT_SETTING_DEFAULT 190 //Current setting is aprox. 4* setting in memory so 190 => 700mA (there is some non-linearity)
#define MEM_BANK208_DIMCURVE_OFFSET_DEFAULT 0x5F
#define MEM_BANK208_CHECKSUM                  ((0 - \
                                                MEM_BANK208_LOCK_BYTE - \
                                                MEM_BANK208_CURRENT_SETTING_DEFAULT - \
                                                MEM_BANK208_DIMCURVE_OFFSET_DEFAULT \
                                                ) % 256)


uint8_t EEbank208mem[MEM_BANK208_SIZE] = {
    MEM_BANK208_SIZE - 1,
    MEM_BANK208_CHECKSUM,
    MEM_BANK208_LOCK_BYTE,
    MEM_BANK208_CURRENT_SETTING_DEFAULT,
    MEM_BANK208_DIMCURVE_OFFSET_DEFAULT        
    };




#endif /* DALI_USE_EXTRA_MEMORY_BANKS */




/** \brief DALI state machine non-volatile memory.
 *
 * The DALI state machine needs some non-volatile memory for it to function
 * according to specifications. It uses nvmem_daliReadByte() and
 * nvmem_daliWriteByte() to read from and write to this memory. The
 * daliMachineEeprom variable implements the memory locations needed for this.
 * DALI_MACHINE_NVMEMORY_REQUIRED represents the number of bytes required by the
 * machine and is defined in dali_cg_machine.h
 */
uint8_t daliMachineEeprom[DALI_MACHINE_NVMEMORY_REQUIRED] =
{
    NVMEMORY_DEFAULT_POWER_ON_LEVEL,
    NVMEMORY_DEFAULT_SYSTEM_FAILURE_LEVEL,
    NVMEMORY_DEFAULT_MINIMUM_LEVEL,
    NVMEMORY_DEFAULT_MAXIMUM_LEVEL,
    NVMEMORY_DEFAULT_FADE_RATE,
    NVMEMORY_DEFAULT_FADE_TIME,
    NVMEMORY_DEFAULT_SHORT_ADDRESS,
    NVMEMORY_DEFAULT_RANDOM_ADDRESS_H,
    NVMEMORY_DEFAULT_RANDOM_ADDRESS_M,
    NVMEMORY_DEFAULT_RANDOM_ADDRESS_L,
    NVMEMORY_DEFAULT_GROUP_LOW,
    NVMEMORY_DEFAULT_GROUP_HIGH,
    NVMEMORY_DEFAULT_SCENES,              // 16 locations

#ifdef DALI_USE_DEVICE_TYPE_6
    NVMEMORY_DEFAULT_FADE_TIME,
    NVMEMORY_DEFAULT_DIMMING_CURVE,
    NVMEMORY_DEFAULT_MULTIPLE_BITS
#endif /* DALI_USE_DEVICE_TYPE_6 */
};

void init_dali_bank_machine_EEP(void)
{
        EEP_Write(EE_DALI_BANK_MACHINE_START_ADDR, MEM_BANK2_SIZE, daliMachineEeprom);
}



// The DALI state machine may need to store the received power level to
// non-volatile memory. As this can be done quite frequently, the DALI state
// machine provides a separate interface, just for this purpose, using the
// functions nvmem_daliReadLastLevel() and nvmem_daliWriteLastLevel(). This
// application example implements a circular buffer 16 bytes long to use wear
// leveling on the EEPROM locations that store these values.
// This value is the circular buffer length.
#define EEPROM_CIRCULAR_BUFFER_SIZE                 0x10


/** \brief Circular buffer that stores the arc power levels in EEPROM.
 *
 */
uint8_t EElastArcValues[EEPROM_CIRCULAR_BUFFER_SIZE];


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Memory storage for reference system power.
 *
 * The reference system power mechanism may need to store values in non-volatile
 * memory. This array can be accessed using the nvmem_refSysPowerReadByte()
 * and nvmem_refSysPowerWriteByte() functions.
 */
//__eeprom uint8_t EEreferenceSystemPower[REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED];

#endif /* DALI_USE_DEVICE_TYPE_6 */



#ifdef DALI_USE_DEVICE_TYPE_6

uint8_t nvmem_refSysPowerReadByte(uint8_t location)
{
    return 0;//EEreferenceSystemPower[location];
}


void nvmem_refSysPowerWriteByte(uint8_t location, uint8_t value)
{
//    EEreferenceSystemPower[location] = value;
}

#endif /* DALI_USE_DEVICE_TYPE_6 */


uint8_t nvmem_daliReadByte(uint8_t location)
{
    uint8_t data;
    EEP_Random_Read((EE_DALI_BANK_MACHINE_START_ADDR + location), 1, &data);
    return data;
//    return daliMachineEeprom[location];
}


void nvmem_daliWriteByte(uint8_t location, uint8_t value)
{
    EEP_Write((EE_DALI_BANK_MACHINE_START_ADDR + location), 1, &value);
//    daliMachineEeprom[location] = value;
}


uint8_t nvmem_daliReadLastLevel()
{
    uint8_t i, readVal;
    for (i = 0; i < EE_DALI_LAST_ARC_LEN; i++)
    {
//        readVal = EElastArcValues[i];
        EEP_Random_Read((EE_DALI_LAST_ARC_START_ADDR  + i), 1, &readVal);
        if (readVal != 0xFF)
        {
            break;
        }
    }
    return readVal;
}


void nvmem_daliWriteLastLevel(uint8_t level)
{
    uint8_t i, lastVal;
    uint8_t erase = 0xFF;
    // Use wear leveling for this task, as it may be updated often
    for (i = 0; i < EE_DALI_LAST_ARC_LEN; i++)
    {
//        lastVal = EElastArcValues[i];
        EEP_Random_Read((EE_DALI_LAST_ARC_START_ADDR + i), 1, &lastVal);
        if (lastVal != 0xFF)
        {
            if (lastVal != level)
            {
                // Erase current position
                EEP_Write((EE_DALI_LAST_ARC_START_ADDR + i), 1, &erase);
                // Move to next position in circular buffer
                i++;
                if (i >= EE_DALI_LAST_ARC_LEN)
                {
                    i = 0;
                }
                // Write current arc value to this position
                EEP_Write((EE_DALI_LAST_ARC_START_ADDR + i), 1, &level);
            }
            break;
        }
    }
    // This final case deals with the possibility that the buffer is empty, as
    // it should leave the factory. In that case, just use the first memory
    // location
    if (lastVal == 0xFF)
    {
//        EElastArcValues[0] = level;
        EEP_Write((EE_DALI_LAST_ARC_START_ADDR), 1, &level);
    }
}


uint16_t nvmem_daliBankMemoryRead(uint8_t bank, uint8_t location)
{   
    uint8_t data;
    if (bank == 0)
    {
        if (location < MEM_BANK0_SIZE)
        {
            return bank0mem[location];
        }
        else
        {
            return BANK_MEMORY_READ_ERROR;
        }
    }
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
    else if (bank == 1)
    {
        if (location < MEM_BANK1_SIZE)
        {
            if (EEP_Random_Read((EE_DALI_BANK1_START_ADDR + location), 1, &data)){
            return ((uint16_t) data);
            }
        }
        else
        {
            return BANK_MEMORY_READ_ERROR;
        }
    }
    else if (bank == 2)
    {
        if (location < MEM_BANK2_SIZE)
        {
            if (EEP_Random_Read((EE_DALI_BANK2_START_ADDR + location), 1, &data)){
            return ((uint16_t) data);
            }
            else{
                return BANK_MEMORY_READ_ERROR;
            }
        }
        else
        {
            return BANK_MEMORY_READ_ERROR;
        }
    }
    else if (bank == 208){
        if (location < MEM_BANK208_SIZE)
        {
            if (EEP_Random_Read((EE_DALI_BANK208_START_ADDR + location), 1, &data)){
            return ((uint16_t) data);
            }
            else{
                return BANK_MEMORY_READ_ERROR;
            }
        }
        else
        {
            return BANK_MEMORY_READ_ERROR;
        }
        
    }
    
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
    // In case we're trying to access memory from a non-existent bank
    // we need to tell this to the DALI State Machine.
    return BANK_MEMORY_READ_ERROR;
}


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

uint8_t nvmem_daliBankMemoryWrite(uint8_t bank, uint8_t location, uint8_t value)
{
    if ((bank == 0))
    {
        return BANK_MEMORY_WRITE_CANNOT_WRITE;
    }
    else if (bank == 1)
    {
        if (location < MEM_BANK1_SIZE)
        {
           EEP_Write((EE_DALI_BANK1_START_ADDR + location), 1, &value);
            
//            EEbank1mem[location] = value;
            if (location == (MEM_BANK1_SIZE - 1))
            {
                return BANK_MEMORY_WRITE_OK_LAST_LOC;
            }
            return BANK_MEMORY_WRITE_OK_NOT_LAST_LOC;
        }
        else
        {
            return BANK_MEMORY_WRITE_CANNOT_WRITE;
        }
    }
    else if (bank == 2)
    {
        if (location < MEM_BANK2_SIZE)
        {
            EEP_Write((EE_DALI_BANK2_START_ADDR + location), 1, &value);
            
//            EEbank2mem[location] = value;
            if (location == (MEM_BANK2_SIZE - 1))
            {
                return BANK_MEMORY_WRITE_OK_LAST_LOC;
            }
            return BANK_MEMORY_WRITE_OK_NOT_LAST_LOC;
        }
        else
        {
            return BANK_MEMORY_WRITE_CANNOT_WRITE;
        }
    }
    else if (bank == 208)
        {
            if (location < MEM_BANK208_SIZE)
            {
                EEP_Write((EE_DALI_BANK208_START_ADDR + location), 1, &value);

    //            EEbank2mem[location] = value;
                if (location == (MEM_BANK208_SIZE - 1))
                {
                    return BANK_MEMORY_WRITE_OK_LAST_LOC;
                }
                return BANK_MEMORY_WRITE_OK_NOT_LAST_LOC;
            }
            else
            {
                return BANK_MEMORY_WRITE_CANNOT_WRITE;
            }
        }
    // If we haven't returned by now, bank unimplemented
    return BANK_MEMORY_WRITE_CANNOT_WRITE;
}

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


inline void init_dali_bank_1_EEP(void){//this should only be used when on first run from factory
//    uint8_t i;
//
//    for (i = 0; i < MEM_BANK1_SIZE; i++)// this way cannot write over page end
//    {
//        EEP_Write((EE_DALI_BANK1_START_ADDR + i), 1, &EEbank1mem[i]);
//    }
//    
//    uint8_t data[20];
    EEP_Write(EE_DALI_BANK1_START_ADDR, MEM_BANK1_SIZE, EEbank1mem);
//    EEP_Random_Read(EE_DALI_BANK1_START_ADDR, MEM_BANK1_SIZE, &data);
//    for (i = 0; i < MEM_BANK1_SIZE; i++)
//    {
//        printf("%u\n", data[i]);
//    }
//       
}

inline void init_dali_bank_2_EEP(void)//this should only be used when on first run from factory
{
        EEP_Write(EE_DALI_BANK2_START_ADDR, MEM_BANK2_SIZE, EEbank2mem);
}

inline void init_dali_bank_208_EEP(void){
    EEP_Write(EE_DALI_BANK208_START_ADDR, MEM_BANK208_SIZE, EEbank208mem);
}



void nvmem_reset(void){
    uint8_t erase = 0xFF;

        EEP_Write(EE_DALI_INITIALISE, 1, &erase);
        EEP_Random_Read(0x0, 1, &erase);
        printf("nvmem erase - %u ", erase);
        printf("\n");
}

void nvmem_dali_banks_init(void){//this should only be used when on first run from factory
    uint8_t data = 0;
    EEP_Random_Read(EE_DALI_INITIALISE, 1, &data);
    
    if(0xFF == data){
       printf("reset nvmem\n");
       init_dali_bank_1_EEP();
       init_dali_bank_2_EEP();
       init_dali_bank_208_EEP();
       init_dali_bank_machine_EEP(); 
    }
    data = 0x0;
    EEP_Write(EE_DALI_INITIALISE, 1, &data);
    
}