/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_nvmemory.h
 *
 * About:
 *  Non-volatile memory prototypes for the DALI library example.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/

#ifndef DALI_CG_NVMEMORY_H
#define	DALI_CG_NVMEMORY_H

#ifdef	__cplusplus
extern "C" {
#endif


#include <stdint.h>
#include "dali_cg_config.h"


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Read a byte from non-volatile memory for reference system power.
 *
 * The reference system power system needs to be able to read/write to
 * non-volatile memory.
 *
 * @param location Location of the byte, in the range [0..REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED-1].
 * @return Value read from non-volatile memory.
 */
uint8_t nvmem_refSysPowerReadByte(uint8_t location);


/** \brief Write a byte to non-volatile memory for reference system power.
 *
 * The reference system power system needs to be able to read/write to
 * non-volatile memory.
 *
 * @param location Location of the byte, in the range [0..REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED-1].
 * @param value Value that needs to be written to non-volatile memory.
 */
void nvmem_refSysPowerWriteByte(uint8_t location, uint8_t value);

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Read a byte from non-volatile memory for the DALI system.
 * 
 * The DALI system needs to store some of its variables to non-volatile memory
 * in order to function according to specifications. Since the example
 * application has access to on-chip EEPROM this is used to implement the
 * non-volatile memory.
 * 
 * @param location Location of the byte, in the range [0..DALI_MACHINE_NVMEMORY_REQUIRED-1].
 * @return Value read from non-volatile memory.
 */
uint8_t nvmem_daliReadByte(uint8_t location);


/** \brief Write a byte to non-volatile memory for the DALI system.
 *
 * The DALI system needs to store some of its variables to non-volatile memory
 * in order to function according to specifications. Since the example
 * application has access to on-chip EEPROM this is used to implement the
 * non-volatile memory.
 * 
 * @param location Location of the byte, in the range [0..DALI_MACHINE_NVMEMORY_REQUIRED-1].
 * @param value Value that needs to be written to non-volatile memory.
 */
void nvmem_daliWriteByte(uint8_t location, uint8_t value);


/** \brief Retrieve the last value written using nvmem_daliWriteLastLevel().
 *
 * In this implementation which uses a circular buffer, this should be the only
 * value in the buffer that differs from 0xFF.
 *
 * @return Last value written using nvmem_daliWriteLastLevel().
 */
uint8_t nvmem_daliReadLastLevel();


/** \brief Write this value to non-volatile memory to be later retrieved using
 * nvmem_daliReadLastLevel().
 *
 * The value written represents the current arc power level and needs to be
 * stored under certain conditions (if POWER ON LEVEL is set to 255). As such,
 * this may be called frequently, thus wearing out the non-volatile memory. To
 * accomodate this a special interface is provided using this pair of functions
 * such that the application developer can implement this in the most efficient
 * way, such as using wear-leveling on multiple locations or only updating the
 * actual level stored to the non-volatile memory after a time-out. \n
 * This implementation uses a circular buffer to store the values, thus
 * distributing EEPROM cell usage over a larger number of cells. We know that
 * the data written to the buffer needs to be a dimming level, and as such this
 * will take values in [0..254]. We can then use the value 255 to mark unused
 * positions in the buffer. Since 255 is an unprogrammed EEPROM cell, this also
 * means we don't do more than one EEPROM programming per write.
 * The function looks for the first programmed value and erases it, moves to the
 * next position (wrapping around at the end of the buffer) and writes the new
 * value there.
 *
 * @param level Value that needs to be written to non-volatile memory.
 */
void nvmem_daliWriteLastLevel(uint8_t level);


/** \brief Read a byte from bank memory.
 *
 * This application example uses 3 memory banks and this function implements the
 * read accesses for these. Given that one bank is in flash and the other two in
 * EEPROM and all have different lenghths, there is an explicit block of code to
 * handle each of the banks.
 * The return type needs to be 16-bit where the upper 8 bits, if different from
 * 0, sigal an error.
 *
 * @param bank Bank from which to read.
 * @param location Memory location in the bank.
 * @return
 *  - If the 8 upper bits are 0, then the return value is the byte read from
 *    the bank memory. \n
 *  - Otherwise, an error is signalled using the value BANK_MEMORY_READ_ERROR
 *    which has the upper 8 bits different from 0. This value represents that
 *    an access past the last location of an implemented bank or of an
 *    unimplemented bank has been attempted.
 */
uint16_t nvmem_daliBankMemoryRead(uint8_t bank, uint8_t location);


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

/** \brief Write a byte to bank memory.
 *
 * This function may should allow any value to be written to EEPROM banks, the
 * DALI system makes sure this memory is used appropriately (memory locks are
 * implemented).
 *
 * @param bank Bank from which to read.
 * @param location Memory location in the bank.
 * @param value Byte that needs to be stored in the memory bank.
 * @return
 *  - BANK_MEMORY_WRITE_OK_NOT_LAST_LOC -- write ok, not last accessible memory
 *    address in bank. \n
 *  - BANK_MEMORY_WRITE_OK_LAST_LOC -- write ok, last accessible memory address
 *    in bank. \n
 *  - BANK_MEMORY_WRITE_CANNOT_WRITE -- cannot write.
 */
uint8_t nvmem_daliBankMemoryWrite(uint8_t bank, uint8_t location, uint8_t value);

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

void nvmem_reset(void);

void nvmem_dali_banks_init(void);

#ifdef	__cplusplus
}
#endif

#endif	/* DALI_CG_NVMEMORY_H */

