/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_hardware.h
 *
 * About:
 *  Hardware definition layer for the DALI library example.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/


#ifndef DALI_CG_HARDWARE_H
#define	DALI_CG_HARDWARE_H

#ifdef	__cplusplus
extern "C" {
#endif


// The timer used to generate the DALI delays is often being reloaded with
// values based on its overflow value and the TE value defined further below.
// Here we use a 16-bit timer, thus its overflow value is 2^16.
#define TE_TIMER_OVERFLOW_VAL           65536


// The number of timer clocks that make up a 416us delay. The timer is
// configured to run at 3MHz, thus our delay will be 3MHz * 416.(6)us = 1250.
    #define TE                              1250
   
   


#include <xc.h>
#include <stdint.h>
#include "dali_cg.h"



/** \brief Turn on Te Timer.
 *
 */
inline void dalihw_teTimerOn();


/** \brief Turn off Te Timer.
 *
 */
inline void dalihw_teTimerOff();


/** \brief Read Te Timer value.
 *
 * @return Te Timer value.
 */
inline uint16_t dalihw_teTimerGetVal();


/** \brief Set Te Timer value.
 *
 * @param val Value to reload Te Timer with.
 */
inline void dalihw_teTimerSetVal(uint16_t val);


/** \brief Set the External Interrupt to fire on the falling edge of the DALI
 * bus.
 *
 * In this application, the "rising" and "falling" in the function names refer
 * to the DALI bus, but there's a layer of inversion in the optocouplers'
 * transistors on the board. Thus, what is called in the function names "rising"
 * is a falling edge on the PIC's pin.
 */
inline void dalihw_extInterruptSetFallingDALI();


/** \brief Set the External Interrupt to fire on the rising edge of the DALI
 * bus.
 *
 * In this application, the "rising" and "falling" in the function names refer
 * to the DALI bus, but there's a layer of inversion in the optocouplers'
 * transistors on the board. Thus, what is called in the function names "rising"
 * is a falling edge on the PIC's pin.
 */
inline void dalihw_extInterruptSetRisingDALI();


/** \brief Toggle the interrupt edge on which the External Interrupt fires.
 *
 */
inline void dalihw_extInterruptToggleEdge();


/** \brief Check whether the DALI line is low or not.
 *
 * This is used to detect cable disconnection (in order to set the lamp power
 * level to system failure level)
 *
 * @return 1 if line is low, 0 otherwise.
 */
inline uint8_t dalihw_isDALILineLow();


/** \brief Pull DALI bus to the low level.
 *
 */
inline void dalihw_setDALILineLow();


/** \brief Release DALI bus such that the IPS can pull it to the high level.
 *
 */
inline void dalihw_setDALILineHigh();


#ifdef	__cplusplus
}
#endif

#endif	/* DALI_CG_HARDWARE_H */

