/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "mcc_generated_files/i2c1.h"
#include "eeprom.h"
/* This section lists the other files that are included in this file.
 */




const uint8_t       testVals[] = {0,4,8,16,64,97,128,253};
const uint8_t       testVals2[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
volatile uint16_t   retryTimeOut, slaveTimeOut;



void EEP_ISR_1Khz(void)
{
    retryTimeOut++;
    slaveTimeOut++;
}

uint8_t EEP_Write(uint16_t address, uint8_t length, uint8_t *data)
{
    
    I2C1_MESSAGE_STATUS status;//initialise as no state
    uint8_t     writeBuffer[66];
    
    if (length > EEP_PAGE_SIZE){
        return ERR_REQ_OVERFLOW;	// over length, exceeds our buffer size.
    }
    
	if ((address + length - 1) > EEP_LAST_ADD)
		return ERR_REQ_ADD_ERR;{
    }
        
	if (address < EEP_FIRST_ADD){
		return ERR_REQ_ADD_ERR;
    }
        
	if ((address >> 6) != ((address + length - 1) >> 6)){	// check we start and finish in the same page.
		printf("PWERR!");
		return ERR_REQ_PAGE_WR_ERR;
	}
    
    // build the write buffer first
    // starting address of the EEPROM memory
    writeBuffer[0] = (address >> 8);                // high address
    writeBuffer[1] = (uint8_t)(address);            // low low address
    memcpy(&writeBuffer[2], data, length);
    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    retryTimeOut = 0;
    
    while(retryTimeOut <  M24256_RETRY_MAX )
    {
        while(status != I2C1_MESSAGE_FAIL)
                        {
                            I2C1_MasterWrite(writeBuffer, (length+2), M24256_ADDRESS, &status);
                            slaveTimeOut = 0;
                            while(status == I2C1_MESSAGE_PENDING)
                            {
                                while(slaveTimeOut < 3); //3ms Delav
                                if (slaveTimeOut > M24256_SLAVE_MAX)
                                {
                                    break;
                                }
                            }
                            if (status == I2C1_MESSAGE_COMPLETE)
                            {
//                                printf("EEw\n");
                                 return(true);
                            }
                        }
    }
                    if (status == I2C1_MESSAGE_FAIL)
                    {
//                        printf("%u \n", retryTimeOut);
//                        printf("f\n");
                        return(false);
                    }
                       
    
}

bool EEP_Random_Read(uint16_t address,  uint8_t length, uint8_t *pdata)
{
    I2C1_MESSAGE_STATUS status; //initialise as no state
    uint8_t     addressBuffer[2];

    // build the address buffer first
    // starting address of the EEPROM memory
    addressBuffer[0] = (address >> 8);                // high address
    addressBuffer[1] = (uint8_t)(address);            // low low address
    
    // Now it is possible that the slave device will be slow.
    // As a work around on these slaves, the application can
    // retry sending the transaction
    retryTimeOut = 0;
    

                        while(status != I2C1_MESSAGE_FAIL || retryTimeOut < M24256_RETRY_MAX)
                        {
                            I2C1_MasterWriteRead(addressBuffer,  2, pdata, length, M24256_ADDRESS, &status);
                            slaveTimeOut = 0;
                            while(status == I2C1_MESSAGE_PENDING)
                            {
                                while(slaveTimeOut < 1); //1ms Delav
                                if (slaveTimeOut > M24256_SLAVE_MAX)
                                {
                                    break;
                                }
                            }
                            
                            if (status == I2C1_MESSAGE_COMPLETE)
                            {
//                                printf("EEr\n");
                                return(true);
                            }
                            
                          
                        }
                        
    
    
                    if (status == I2C1_MESSAGE_FAIL )
                    {
                        return(false);
                    }
                        
}







void eepTest(void){
    uint8_t test[8];
    uint8_t i;


        EEP_Write(0x3FF7, 8, &testVals2);
        EEP_Random_Read(0x3FF7, 8, test);
        for (i = 0; 8 > i; i++)
        {
            printf("%u ",test[i]);
        }
        printf("\n");
//        if (0 == memcmp(test, testVals2, 8)){ // memory matched, we're done.
//            
//		printf("eep");}
//        else{
//            printf("eepfail\n");}

        EEP_Write(0x3FF7, 8, &testVals);
        EEP_Random_Read(0x3FF7, 8, test);
                for (i = 0; 8 > i; i++)
        {
            printf("%u ",test[i]);
        }
        printf("\n");
                
//        if (0 == memcmp(test, testVals, 8)){ // memory matched, we're done.
//		printf("pass\n");}
//                else{
//        printf("fail\n");
//                }
}



//// Don't write across pages!
//uint8_t eepWriteBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count){
//
//	uint8_t formatted[66];			//where we combine startAddress and the buffer, since a page is 64 butes, 66 is as big as we'll ever need this.
//	uint8_t result;
//
//	if (count > EEP_PAGE_SIZE)
//		return ERR_REQ_OVERFLOW;	// over length, exceeds our buffer size.
//
//	if ((startAddress + count - 1) > EEP_LAST_ADD)
//		return ERR_REQ_ADD_ERR;
//
//	if (startAddress < EEP_FIRST_ADD)
//		return ERR_REQ_ADD_ERR;
//
//	if ((startAddress >> 6) != ((startAddress + count - 1) >> 6)){	// check we start and finish in the same page.
//		printf("PWERR!");
//		return ERR_REQ_PAGE_WR_ERR;
//	}
//
//	if (I2C1_MESSAGE_COMPLETE != I2C_Write(&result, 1 ,(EEP_I2C_ADDRESS << 1))){
//		return ERR_REQ_BUSY;		// things are busy, try later.
//	}
//
//	formatted[1] = (0x00ff & startAddress);	// copy the address and flip the endianness.
//	formatted[0] = (startAddress >> 8);
//	memcpy(&formatted[2], buffer, count);	// copy the data into our local array, skipping the first two address bytes, we setup above
//
////	eepClearWriteProtect();
////	result = halI2cWriteBytes((EEP_I2C_ADDRESS << 1), formatted, count + 2, 0);	// +2 for address.
//    result = I2C_Write(formatted, 3 ,(EEP_I2C_ADDRESS << 1));
////	eepSetWriteProtect();
//	return result;		// return the result of the eeprom write.
//}
//
//
//uint8_t eepReadBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count){
//
//	//uint8_t formatted[66];
//	uint8_t address[2];
//	uint8_t result;
//
//	if (count > EEP_MAX_SIZE){
//		return ERR_REQ_OVERFLOW;	// over length, exceeds our buffer size.
//	}
//
//	if ((startAddress + count - 1) > EEP_LAST_ADD)
//		return ERR_REQ_ADD_ERR;
//
//	if (startAddress < EEP_FIRST_ADD)
//		return ERR_REQ_ADD_ERR;
//
//	// First test to see if the EEPROM is busy or not.
//	if (I2C1_MESSAGE_COMPLETE != I2C_Write(&result, 1 ,(EEP_I2C_ADDRESS << 1))){
//		return ERR_REQ_BUSY;		// things are busy, try later.
//	}
//
//	address[1] = (0x00ff & startAddress);	// copy the address and flip the endianness.
//	address[0] = (startAddress >> 8);
////	result = halI2cWriteBytes((EEP_I2C_ADDRESS << 1), address, 2, 0);
//    result = I2C_Write(address, 2 (EEP_I2C_ADDRESS << 1));
//
//	if (result != I2C1_MESSAGE_COMPLETE){
//		return result;		// failure, don't go any further
//	}
//
//	result = I2C_Read((EEP_I2C_ADDRESS << 1), (uint8_t*) buffer, count, 0);		// Clock the data out and into the newScene structure.
//	return result;
//}

