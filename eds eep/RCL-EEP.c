#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include EMBER_AF_API_STACK
#include EMBER_AF_API_EMBER_TYPES
#include EMBER_AF_API_HAL
//#include EMBER_AF_API_SIM_EEPROM
#ifdef EMBER_AF_API_DEBUG_PRINT
  #include EMBER_AF_API_DEBUG_PRINT
#endif

#include EMBER_AF_API_SERIAL
#include <stdio.h>
#include <string.h>
#include "i2c-driver/EFR32/i2c-driver.h"
#include "RCL-EEP.h"


const uint8_t testVals[] = {0,4,8,16,64,97,128,253};

void eepSetWriteProtect(void){
	GPIO_PinOutSet(gpioPortF, 6);
}

void eepInit(void){
	// Pins are setup in RCL-Pins-And-Status

	eepSetWriteProtect();									// Make sure the pin is set high for write protect.
	halI2cInitialize();
}

void eepClearWriteProtect(void){
	GPIO_PinOutClear(gpioPortF, 6);
}
// Don't write across pages!
uint8_t eepWriteBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count){

	uint8_t formatted[66];			//where we combine startAddress and the buffer, since a page is 64 butes, 66 is as big as we'll ever need this.
	uint8_t result;

	if (count > EEP_PAGE_SIZE)
		return ERR_REQ_OVERFLOW;	// over length, exceeds our buffer size.

	if ((startAddress + count - 1) > EEP_LAST_ADD)
		return ERR_REQ_ADD_ERR;

	if (startAddress < EEP_FIRST_ADD)
		return ERR_REQ_ADD_ERR;

	if ((startAddress >> 6) != ((startAddress + count - 1) >> 6)){	// check we start and finish in the same page.
		emberAfAppPrintln("PWERR!");
		return ERR_REQ_PAGE_WR_ERR;
	}

	if (I2C_DRIVER_ERR_NONE != halI2cReadBytes((EEP_I2C_ADDRESS << 1), &result, 1, 0)){
		return ERR_REQ_BUSY;		// things are busy, try later.
	}

	formatted[1] = (0x00ff & startAddress);	// copy the address and flip the endianness.
	formatted[0] = (startAddress >> 8);
	memcpy(&formatted[2], buffer, count);	// copy the data into our local array, skipping the first two address bytes, we setup above

	eepClearWriteProtect();
	result = halI2cWriteBytes((EEP_I2C_ADDRESS << 1), formatted, count + 2, 0);	// +2 for address.
	eepSetWriteProtect();
	return result;		// return the result of the eeprom write.
}


uint8_t eepReadBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count){

	//uint8_t formatted[66];
	uint8_t address[2];
	uint8_t result;

	if (count > EEP_MAX_SIZE){
		return ERR_REQ_OVERFLOW;	// over length, exceeds our buffer size.
	}

	if ((startAddress + count - 1) > EEP_LAST_ADD)
		return ERR_REQ_ADD_ERR;

	if (startAddress < EEP_FIRST_ADD)
		return ERR_REQ_ADD_ERR;

	// First test to see if the EEPROM is busy or not.
	if (I2C_DRIVER_ERR_NONE != halI2cReadBytes((EEP_I2C_ADDRESS << 1), &result, 1, 0)){
		return ERR_REQ_BUSY;		// things are busy, try later.
	}

	address[1] = (0x00ff & startAddress);	// copy the address and flip the endianness.
	address[0] = (startAddress >> 8);
	result = halI2cWriteBytes((EEP_I2C_ADDRESS << 1), address, 2, 0);

	if (result != I2C_DRIVER_ERR_NONE){
		return result;		// failure, don't go any further
	}

	result = halI2cReadBytes((EEP_I2C_ADDRESS << 1), (uint8_t*) buffer, count, 0);		// Clock the data out and into the newScene structure.
	return result;
}

uint8_t eepTest(void){

	uint8_t testArray[8];
//	uint8_t result;
//	uint8_t attempts = 0;


	if (I2C_DRIVER_ERR_NONE != eepReadBytes(EEP_MAGIC_BYTES, testArray, 8))
		return false;


	if (0 == memcmp(testArray, testVals, 8)) // memory matched, we're done.
		return true;


	if (I2C_DRIVER_ERR_NONE != eepWriteBytes(EEP_MAGIC_BYTES, testVals, 8))
		return false;

	return true;
}
