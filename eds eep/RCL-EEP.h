/*
 * RCL-EEP.h
 *
 *  Created on: 19 Apr 2018
 *      Author: edpgc
 */
#ifndef RCL_EEP_H_
#define RCL_EEP_H_

#define     lo8(x) ((x)&0xff)
#define     hi8(x) ((x)>>8)

#define	EEP_I2C_ADDRESS			0x50


// Note these are aligned with the I2C driver.

#define ERR_NONE         		0x00
#define ERR_TIMEOUT      		0x01
#define ERR_ADDR_NAK     		0x02
#define ERR_DATA_NAK     		0x03
#define ERR_ARB_LOST     		0x04
#define ERR_USAGE_FAULT  		0x05
#define ERR_SW_FAULT     		0x06
#define ERR_UNKOWN       		0xFF

// We've added these errors, these occur seperately to i2c errors
#define ERR_REQ_OVERFLOW		0x80
#define ERR_REQ_ADD_ERR			0x82
#define ERR_REQ_PAGE_WR_ERR		0x83
#define ERR_REQ_BUSY			0x81		// if you get busy then retry - this in a non fatal state.


#define EEP_PAGE_SIZE       	0x40			// Do not try and write across pages!
#define EEP_MAX_SIZE       		0x40			//
#define EEP_FIRST_ADD			0x0000			// First address we allow write and reads too
#define EEP_LAST_ADD			0x7FFF			// Last address we allow write and reads too.

void eepInit(void);
uint8_t eepWriteBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count);
uint8_t eepReadBytes(uint32_t startAddress, const uint8_t *buffer, uint8_t count);
uint8_t eepTest(void);


#endif /* RCL_EEP_H_ */

#define EEP_MAGIC_BYTES				0x00000100			// 16 bytes are held here


/* EEPROM address space usage */
//
//
//#define EEP_GROUP_START			0x00000400			//
//#define EEP_FIRST_SCN				0x00001000			// 		Scenes start at 0x00001000
